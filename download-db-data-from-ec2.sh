#!/bin/bash

# Program do download the database files from remote EC2 nodes
echo "Started downloading data from remote EC2 server - $(date)"
for host in `awk '/db/ {print $3}' hosts.txt`
do
    echo Copying from $host
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem root@$host:/disk/xvdg/completed-backup* .
done

# Print details
echo "Completed. All backups are present under $(pwd)/completed-backups/ directory";
ls -l $(pwd)/completed-backups/

# print status
echo "All completed - $(date)."
