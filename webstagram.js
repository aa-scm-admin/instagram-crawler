/*
 * Author: Naresh <nareshv@colligent.com
 * Date  : Feb-2014
 *
 * (c) 2014 Naresh. All Rights Reserved.
 */

var request    = require('request');
var cheerio    = require('cheerio');
var string     = require('string');
var Sequelize  = require('sequelize');
var mysql      = require('mysql');
var kue        = require('kue');
var fs         = require('fs');
var FeedParser = require('feedparser');
var crypto     = require('crypto');
var uuid       = require('node-uuid');
var cache      = require('./my-cache');
var options    = require('./my-options');
var next_date  = new Date();
var stack      = { add : 0, remove : 0 };
next_date.setSeconds(next_date.getSeconds() + (options.crawl_options.delay/1000));

var InstagramProfile = {
    get_rss_url: function(profile) {
        return 'http://widget.stagram.com/rss/n/' + profile + '/';
    },
    get_ajax_url: function(url) {
        if (url) {
            return url.replace(/\/p\//, '/get_comments/');
        } else {
            return 'http://web.stagram.com/';
        }
    }
};

var top100 = options.top100;
//var top100 = [ 'arianagrande' ];
//

var pool = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'web_stagram'
});
var Job       = kue.Job;
var jobs      = kue.createQueue();

kue.app.listen(3000);
kue.app.set('title', 'WebStagram');

var sequelize = new Sequelize('mysql://web_stagram:webstagrampass@localhost/web_stagram', {
    dialect : 'mysql',
    logging : false,
    pool    : { maxConnections: 5, maxIdleTime: 30},
    define  : {
        timestamps: true,
        charset: 'utf8',
        collate: 'utf8_general_ci'
    },
    sync: { force : true }
});

var RSSFeed      = sequelize.import(__dirname + "/models/RSSFeed.js");
var RSSFeedItem  = sequelize.import(__dirname + "/models/RSSFeedItem.js");
var RSSFeedItemComment  = sequelize.import(__dirname + "/models/RSSFeedItemComment.js");

// Create the tables
var force = { force : true};
if (process.env.NODE_ENV == "production") {
    force = { force : false };
}
RSSFeed.sync(force);
RSSFeedItem.sync(force);
RSSFeedItemComment.sync(force);

//var url = 'http://widget.stagram.com/rss/n/backstreetboys/';
var parseFeed = function(url, job, done) {
    //var feed = __dirname + '/' + profile + '.rss.xml';
    var obj  = null;

    console.log("\nProcessing job -> " + url);

    // Inject this url into the "RSSFeeds" table
    RSSFeed.findOrCreate({
        url : url,
        profile_id : job.data.profile_id,
        xml : null,
        creator : null,
        ctime : null,
        num_items : 0
    }).success(function(rssfeed, rssfeedchannel) {
        var num_items = 0;

        if (url.match(/^http/)) {
            obj = request({ uri: url, headers : options.headers})
                  .pipe(new FeedParser())
        } else {
            obj = fs.createReadStream(feed)
            .on('error', function (error) {
                console.error(error);
            })
            .pipe(new FeedParser())
        }

        // After we get the response, get more urls from the rss
        // feed
        var handleData = function() {
            var stream = this, item;
            while (item = stream.read()) {
                if (item !== null && item !== undefined) {
                    RSSFeedItem.findOrCreate({ rss_id : rssfeed.id },
                        {
                            rss_id       : rssfeed.id,
                            link         : item.link,
                            title        : item.title,
                            language     : item.meta.language,
                            author       : item.meta.author,
                            date         : item.date,
                            description  : item.description,
                            num_comments : 0
                        }
                    ).success(function(created, channel) {
                        num_items++;
                        console.log("== New profile is created. %s", created.link)
                        // console.log("== New profile is created. Below are the details ==");
                        // console.log("Link          : %s", created.link);
                        // console.log("Description   : %s", ''); //created.description);
                        // console.log("Title         : %s", created.title);
                        // console.log("Language      : %s", created.language);
                        // console.log("Author        : %s", created.author);
                        // console.log("Created Time  : %s", created.date);
                        // Primary key is needed for the association
                        var attrs = {
                            db_id : created.id
                        };
                        // Schedule this new page/item-in-RSS for future crawl
                        schedule_profile(job.data.profile_id, created.link, "item", attrs);
                        // Increment the num items
                        rssfeed.num_items = num_items;
                        rssfeed.save();
                    }).error(function(error) {
                        console.log("Error while adding %s", error);
                        done(error);
                    });
                }
            }
        }

        obj
        .on('error', function (error) {
            console.error(error);
        })
        .on('meta', function (meta) {
            console.log('===== %s =====', meta.title);
        })
        .on('readable', handleData)
        .on('end', function() {
            stack.remove++;
            done();
        });
    });
};

var parseFeedItem = function(url, referer, item_id, job, done) {
    // Make an ajax call like
    // Original http://web.stagram.com/p/628328467651519641_53027200
    // Ajax     http://web.stagram.com/get_comments/628328467651519641_53027200
    //
    // Make sure to set these headers
    // Referer: $Original
    // X-Requested-With: XMLHttpRequest
    if (!url.match(/\/get_comments\//) || !referer.match(/\/p\//)) {
        console.log("[debug] Original => " + url);
        console.log("[debug] Referer  => " + referer);
        throw "Incorrect URL/Refer URL";
    }
    request({ uri: url, headers : options.AJAXheaders(referer) }, function(err, resp, body) {
        if (err)
            throw err
        var r = JSON.parse(body);
        if (r.status == "OK") {
            // zero is special index where there are all fans.
            // so start from '1'
            for (var i = 1; i < r.message.length; i++) {
                RSSFeedItemComment.findOrCreate({
                    item_id              : item_id,
                    comment_id           : r.message[i].id,
                    created_time         : r.message[i].created_time,
                    text                 : r.message[i].text,
                    from_username        : r.message[i].from.username,
                    from_profile_picture : r.message[i].from.profile_picture,
                    from_id              : r.message[i].from.id,
                    from_full_name       : r.message[i].from.full_name,
                }).success(function(created, channel) {
                    // console.log("[%s] == New Comment is Added ==", job.data.profile_id)
                    // console.log("[%s] Item ID        : %s", job.data.profile_id, created.item_id);
                    // console.log("[%s] Comment ID     : %s", job.data.profile_id, created.comm_id);
                    // console.log("[%s] Relative Time  : %s", job.data.profile_id, created.created_time);
                    // console.log("[%s] Text           : %s", job.data.profile_id, created.text);
                    // console.log("[%s] Username       : %s", job.data.profile_id, created.from_username);
                    // console.log("[%s] Picture        : %s", job.data.profile_id, created.from_profile_picture);
                    // console.log("[%s] User ID        : %s", job.data.profile_id, created.from_id);
                    // console.log("[%s] User Full name : %s", job.data.profile_id, created.from_full_name);
                    // console.log('');
                    stack.remove++;
                    if (done) 
                        done();
                });
            }
            console.log("[%s] Added %d comments", job.data.profile_id, r.message.length - 1);

            // Update the Actual parent with number of comments found
            RSSFeedItem.find(item_id).success(function(item) {
                item.num_comments += r.message.length - 1;
                item.save()
                .success(function(newitem) {
                    console.log("[rssfeeditem] Updated %s with %d elements", newitem.id, newitem.num_comments);
                })
                .error(function(e) {
                    console.log("[rssfeeditem] Failed to update %s. Error : %s", item.id, e);
                });
            });
        }
    });
};

//parseFeed('ladygaga');
//parseFeedItem(
//    'http://web.stagram.com/get_comments/628328467651519641_53027200',
//    'http://web.stagram.com/p/628328467651519641_53027200',
//    20
//);

jobs.on('job complete', function(id) {
    Job.get(id, function(err, job){
        if (err) return;
        job.remove(function(err){
            if (err) throw err;
            console.log('removed completed job #%d', job.id);
        });
    });
});

jobs.process("url", function(job, done) {
    var url = job.data.url;
    if (url) {
        console.log("[%s] ===> Wokeup after %d seconds! Job %s", job.data.profile_id, job.data.delay, job.data.url);
        if (job.data.type === "item") {
            console.log("[%s] Going to parse item [url]      -> %s", job.data.profile_id, url);
            console.log("[%s] Going to parse item [referrer] -> %s", job.data.profile_id, job.data.referer_url);
            console.log("[%s] Going to parse item            -> %s", job.data.profile_id, job.data.attr_db_id);
            parseFeedItem(url, job.data.referer_url, job.data.attr_db_id, job, done)
        } else if (job.data.type === "profile") {
            console.log("[%s] Going to parse profile -> %s", job.data.profile_id, url);
            parseFeed(url, job, done)
        } else {
            console.log("[%s] Unkown job type -> %s", job.data.profile_id, job.data.type);
        }
    } else {
        console.log("[%s] Invalid Job URL", job.data.profile_id);
    }
});

var schedule_profile = function(profile_id, url, type, attrs) {
    var result = {};
    if (type === "item") {
        result = {
            profile_id  : profile_id,
            url         : InstagramProfile.get_ajax_url(url),
            referer_url : url,
            type        : "item",
            response    : "json",
            title       : 'ITEM/AJAX -> ' + profile_id
        };
    } else {
        result = {
            profile_id : profile_id,
            url        : InstagramProfile.get_rss_url(profile_id),
            type       : "profile",
            response   : "rss",
            title      : 'MAIN/RSS -> ' + profile_id
        };
    }
    // Save the attributes into the job event
    for (var aa in attrs) {
        result['attr_' + aa] = attrs[aa];
    }
    // Give a UUID to job ?
    result.id = uuid.v1();

    // Add this to the queue
    next_date.setSeconds(next_date.getSeconds() + (options.crawl_options.delay/1000));
    var delay = parseInt(next_date.getTime()) - parseInt(new Date().getTime());
    // Add the delay to the actual job to get an idea
    result.delay = delay;
    // Create the actual job
    jobs.create('url', result).priority('high').delay(delay).save();
    console.log("[schedule] Scheduling %s for %s in type %s after %d usecs", profile_id, url, type, delay);
    // Stack variable
    stack.add++;
}

// Initialize the original queue
for (var i = 0, delay = 0; i < top100.length; i++) {
    schedule_profile(top100[i], InstagramProfile.get_rss_url(top100[i]), 'profile');
}

// Start the work for delayed jobs
jobs.promote();

// graceful exit
process.once( 'SIGTERM', function ( sig ) {
  jobs.shutdown(function(err) {
    console.log( 'Kue is shut down.', err||'' );
    process.exit( 0 );
  }, 5000 );
});

// Keep watching for empty queue
var queue_empty_secs = 0;
setInterval(function() {
    console.log("Current Queue. Added = %d, Removed = %d, Balance = %d", stack.add, stack.remove, stack.add - stack.remove);
    if (stack.add == stack.remove) {
        queue_empty_secs += 10000;
    }
    if (queue_empty_secs >= 300 * 1000) {
        console.log("Process is idle for more than 300 seconds. exiting...");
        process.exit(0);
    }
}, 10000);
