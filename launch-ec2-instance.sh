#!/bin/bash

# Author: Naresh
# Date  : Aug/2014

if [[ -z "$1" ]];then
    echo "syntax: $0 <create|destroy>"
    exit
fi
ARG_MODE=$1

export AWS_DEFAULT_OUTPUT="json"

# http://docs.aws.amazon.com/cli/latest/reference/ec2/

#
# Create a new crawl node
# $1 - name of the new crawl node
#
crawlNode() {
    # This is Fedora 20 HVM AMI
    #CRAWL_AMI=ami-bb225c8b
    # This is CENTOS 7 AMI
    CRAWL_AMI=ami-c7d092f7
    CRAWL_ITYPE=t2.micro
    # Basic error checking
    if [[ -z $1 || -z $2 ]];then
        echo "PROGRAMMING-ERROR: You should pass the new crawl node."
        exit;
    else
        # Setup required variables
        NAME="$1"
        MODE="$2"
        CLOG="$NAME.create.log"
        TLOG="$NAME.tag.log"
        FLOG="$NAME.firewall.log"
        RLOG="$NAME.terminate.log"
        WLOG="$NAME.public_ip_wait.log"
        if [ "${MODE}x" == "CREATEx" ];then
            if [ -f "$CLOG" ];then
                echo "ERROR: $CLOG - already exists. Preventing error in re-creating the node ..."
            else
                # Create a new crawl instance
                echo -e "1. Provisioning new server '$NAME' from '$CRAWL_AMI' : \c"
                aws ec2 run-instances --image-id $CRAWL_AMI  --instance-type $CRAWL_ITYPE --key-name affinityanswers  --security-group-ids sg-ef3d9a8a --count 1 --block-device-mappings '[{ "DeviceName": "/dev/xvda", "Ebs": { "VolumeSize": 8, "VolumeType": "standard", "DeleteOnTermination": true } } ]' > "$CLOG" 2>&1
                if [ $? -eq 0 ];then
                    echo "[success] ($CLOG)"
                else
                    echo "[failure] ($CLOG)"
                    exit
                fi

                # Extract required attributes from the log
                instance_id=$(cat "$CLOG" | jq '.Instances[].InstanceId' | sed -e 's/"//g');
                instance_private_ip=$(cat "$CLOG" | jq '.Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress' | sed -e 's/"//g');
                found_public_ip="false"
                found_public_ip_iters=0
                SLEEP=5
                echo -e "2. Waiting for Public IP allocation for $instance_id and $instance_private_ip : \c"
                while [ $found_public_ip == "false" ]
                do
                    instance_public_ip=$(aws ec2 describe-instances --instance-ids "$instance_id" 2>/dev/null | tee "$WLOG" | jq '.Reservations[].Instances[].PublicIpAddress' | sed -e 's/"//g')
                    let "found_public_ip_iters += 1"
                    if [[ -z $instance_public_ip || $instance_public_ip == "null" ]];then
                        if [ $found_public_ip_iters -gt 12 ];then
                            echo "[failure] (log = $WLOG)";
                            echo "ERROR: Unable to locate the Public IP Address after 1 minute. Aborting ..."
                        fi
                        #echo "INFO: Public IP Not available yet. Sleeping for $SLEEP seconds"
                        echo -n "."
                        found_public_ip="false"
                        sleep $SLEEP
                    else
                        found_public_ip="true"
                        echo "[success] (public-ip = $instance_public_ip, log = $WLOG)"
                    fi
                done

                # Make sure we are changing correct things
                if [[ -z $instance_id || -z $instance_private_ip ]];then
                    echo "ERROR: Unable to determine instance id from $CLOG"
                else
                    # After we get the instance id we need to tag it
                    echo -n "3. Assigning hostname : "
                    aws ec2 create-tags --resources $CRAWL_AMI $instance_id --tags "Key=Name,Value=$NAME" 2>&1 > "$TLOG";
                    if [ $? -eq 0 ];then
                        echo "[success] (name = $NAME, log = $TLOG)"
                    else
                        echo "[failure] (name = $NAME, log = $TLOG)"
                    fi

                    # Add the current private ip address to the default security group & remove the old one (at the end of termination ?)
                    # http://docs.aws.amazon.com/cli/latest/reference/ec2/authorize-security-group-ingress.html
                    echo -e "4. Setting up firewall rules : \c"
                    for PORT in 3306 6379
                    do
                        echo -e " [$PORT : \c"
                        aws ec2 authorize-security-group-ingress --group-id sg-ef3d9a8a --protocol tcp --port $PORT --cidr "$instance_private_ip/32" > "$FLOG-$PORT-grant" 2>&1;
                        if [ $? -eq 0 ];then
                            echo -n "success]"
                        else
                            echo -n "failure]"
                        fi
                    done
                fi
                echo

                # Print the hostname <private-ip> <public-ip> on screen
                #echo "$NAME $instance_private_ip $instance_public_ip"
            fi
        elif [ "${MODE}x" == "DELETEx" ];then
            if [ ! -f "$CLOG" ];then
                echo "Error $CLOG does not exist. Aborting"
                exit
            else
                if [[ ! -e "RLOG" ]];then
                    # Extract required attributes from the log
                    instance_id=$(cat "$CLOG" | jq '.Instances[].InstanceId' | sed -e 's/"//g')
                    instance_private_ip=$(cat "$CLOG" | jq '.Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress' | sed -e 's/"//g');

                    # Make sure we are changing correct things
                    if [[ -z $instance_id || -z $instance_private_ip ]];then
                        echo "ERROR: Unable to determine instance id from $CLOG"
                        exit
                    else
                        echo -e "Removing Security Rules for $instance_id and $instance_private_ip : \c"
                        # Remove the current private ip address to the default security group
                        # http://docs.aws.amazon.com/cli/latest/reference/ec2/revoke-security-group-ingress.html
                        for PORT in 3306 6379
                        do
                            echo -e " (port $PORT \c"
                            aws ec2 revoke-security-group-ingress --group-id sg-ef3d9a8a --protocol tcp --port $PORT --cidr "$instance_private_ip/32" > "$FLOG-$PORT-revoke" 2>&1;
                            if [ $? -eq 0 ];then
                                echo -n "removed)"
                            else
                                echo -n "not-removed)"
                            fi
                        done
                        echo ""

                        # Terminate the instance
                        echo -e "Deleting $NAME with $instance_id : \c"
                        aws ec2 terminate-instances --instance-ids $instance_id > "$RLOG" 2>&1;
                        if [ $? -eq 0 ];then
                            echo "[success] (log = $RLOG)";
                        else
                            echo "[failure] (log = $RLOG)"
                        fi
                        echo
                    fi
                fi
            fi
        else
            echo "ERROR: Unknown Mode ($MODE). Aborting."
        fi
    fi
}

#
# Create a new database node
#
databaseNode() {
    # CentOS 6.4 Marketplace AMI
    DB_AMI=ami-b3bf2f83
    DB_ITYPE=m3.xlarge
    # Basic error checking
    if [[ -z $1 || -z $2 ]];then
        echo "PROGRAMMING-ERROR: You should pass the new crawl node."
        exit;
    else
        # Setup required variables
        NAME="$1"
        MODE="$2"
        CLOG="$NAME.create.log"
        TLOG="$NAME.tag.log"
        FLOG="$NAME.firewall.log"
        RLOG="$NAME.terminate.log"
        WLOG="$NAME.public_ip_wait.log"
        if [ "${MODE}x" == "CREATEx" ];then
            if [ -f "$CLOG" ];then
                echo "ERROR: $CLOG - already exists. Preventing error in re-creating the node ..."
            else
                echo -e "1. Provisioning new server '$NAME' from '$DB_AMI' : \c"
                # Create a new db instance
                STORAGE='[ { "DeviceName": "/dev/sda", "Ebs": { "DeleteOnTermination": true } }, { "DeviceName" : "/dev/sdb", "VirtualName" : "ephemeral0" }, { "DeviceName" : "/dev/sdc", "VirtualName" : "ephemeral1" } ]'
                aws ec2 run-instances --block-device-mappings "$STORAGE" --image-id $DB_AMI  --instance-type $DB_ITYPE --key-name affinityanswers  --security-group-ids sg-ef3d9a8a --count 1 > "$CLOG" 2>&1
                if [ $? -eq 0 ];then
                    echo "[success] ($CLOG)"
                else
                    echo "[failure] ($CLOG)"
                    exit
                fi
            fi

            instance_id=$(cat "$CLOG" | jq '.Instances[].InstanceId' | sed -e 's/"//g');
            instance_private_ip=$(cat "$CLOG" | jq '.Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress' | sed -e 's/"//g');

            if [ -f "$WLOG" ];then
                echo "ERROR: $WLOG - already exists. IP Addresses are allocated"
            else
                # Extract required attributes from the log
                found_public_ip_iters=0
                found_public_ip="false"
                echo -e "2. Waiting for Public IP allocation for $instance_id and $instance_private_ip : \c"
                SLEEP=5
                while [ $found_public_ip == "false" ]
                do
                    instance_public_ip=$(aws ec2 describe-instances --instance-ids $instance_id 2>/dev/null | tee "$WLOG" | jq '.Reservations[].Instances[].PublicIpAddress' | sed -e 's/"//g')
                    let "found_public_ip_iters += 1"
                    if [[ -z $instance_public_ip || $instance_public_ip == "null" ]];then
                        if [ $found_public_ip_iters -gt 12 ];then
                            echo "[failure] (log = $WLOG)";
                            echo "ERROR: Unable to locate the Public IP Address after 1 minute. Aborting ..."
                            exit;
                        fi
                        #echo "INFO: Public IP Not available yet. Sleeping for $SLEEP seconds"
                        echo -n "."
                        found_public_ip="false"
                        sleep $SLEEP
                    else
                        found_public_ip="true"
                        echo "[success] (public-ip = $instance_public_ip, log = $WLOG)"
                    fi
                done
            fi

            if [[ -f "$TLOG" && -s "$TLOG" ]];then
                echo "ERROR: $TLOG - already exists. Skipping tagging .."
            else
                # Make sure we are changing correct things
                if [[ -z $instance_id || -z $instance_private_ip ]];then
                    echo "ERROR: Unable to determine instance id from $CLOG"
                else
                    # After we get the instance id we need to tag it
                    echo -n "3. Assigning hostname $NAME : "
                    aws ec2 create-tags --resources $DB_AMI $instance_id --tags "Key=Name,Value=$NAME" 2>&1 > "$TLOG";
                    if [ $? -eq 0 ];then
                        echo "[success] (name = $NAME, log = $TLOG)"
                    else
                        echo "[failure] (name = $NAME, log = $TLOG)"
                    fi
                fi
                echo
            fi
        elif [ "${MODE}x" == "DELETEx" ];then
            if [ ! -f "$CLOG" ];then
                echo "Error $CLOG does not exist. Perhaps no instance is created ?. Aborting"
                exit
            else
                if [[ ! -e "RLOG" ]];then
                    # Extract required attributes from the log
                    instance_id=$(cat "$CLOG" | jq '.Instances[].InstanceId' | sed -e 's/"//g')

                    # Make sure we are changing correct things
                    if [[ -z $instance_id ]];then
                        echo "ERROR: Unable to determine instance id from $CLOG"
                        exit
                    else
                        # Terminate the instance
                        echo -e "Deleting $NAME with $instance_id : \c"
                        aws ec2 terminate-instances --instance-ids $instance_id > "$RLOG" 2>&1;
                        if [ $? -eq 0 ];then
                            echo "[success] (log = $RLOG)";
                        else
                            echo "[failure] (log = $RLOG)"
                        fi
                    fi
                else
                    echo "ERROR: $RLOG File already exists. Below are its contents."
                    cat $RLOG
                fi
            fi
        else
            echo "ERROR: Unknown Mode ($MODE). Aborting."
        fi
    fi
}

printHosts() {
    echo ""
    echo ""
    for NAME in crawl1 crawl2 crawl3 crawl4 crawl5 db
    #for NAME in crawl6 crawl7 crawl8 crawl9 crawl10
    do
        WLOG="$NAME.public_ip_wait.log"
        if [ -e "$WLOG" ];then
            instance_public_ip=$(cat "$WLOG" | jq '.Reservations[].Instances[].PublicIpAddress' | sed -e 's/"//g')
            instance_private_ip=$(cat "$WLOG" | jq '.Reservations[].Instances[].PrivateIpAddress' | sed -e 's/"//g')
            echo "$NAME $instance_private_ip $instance_public_ip"
        else
            echo "# ERROR: $WLOG -- missing (`date`)"
        fi
    done
}


# Create the database node
if [ $ARG_MODE == "create" ];then
    databaseNode "db" "CREATE"
    # Start all the crawl nodes
    for Q in 1 2 3 4 5
    do
        crawlNode "crawl$Q" "CREATE"
    done
    # print all the hosts
    printHosts | tee -a hosts-generated.txt
elif [ $ARG_MODE == "delete" ];then
    databaseNode "db" "DELETE"
    # Start all the crawl nodes
    for Q in 1 2 3 4 5
    do
        crawlNode "crawl$Q" "DELETE"
    done
    # print all the hosts
    printHosts | tee -a hosts-generated.txt
else
    echo "invalid mode $ARG_MODE"
fi

