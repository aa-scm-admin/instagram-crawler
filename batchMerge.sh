#!/bin/sh


#for every week 
#tail +1 weeklist | while read line 
#head -2 weeklist | tail -1 | while read line 
cat weeklist | while read line 
do
# Get the Week
echo `date` : $line 

# Get the latest count of with and without Null from the respective Week tables
echo "--- `date` $line pre counts";
echo "select count(*) as null_counts from instagram.entity_fan_ALL_master where last_act_time is NULL " | mysql -t -uinstagram -hstage5 -pinstagrampass -A
echo "select count(*) as non_null_counts from instagram.entity_fan_ALL_master where last_act_time is NOT NULL " | mysql -t -uinstagram -hstage5 -pinstagrampass -A
# Set the DB Status
#	Set the Job_Entity

echo "`date` Changing the status to the crawled state ...";
echo "update job_entities_$line set status = 'crawled' where status = 'merged';" | mysql -uinstagram -hstage5 -A instagram_crawl -pinstagrampass -t

# copy the respective week json file
cp weekly-jsonfiles/Instagram$line.json Instagram.json

#execute the start-merge for that week
mkdir -p $line-merge-fornull && ./start-merge.sh $line-merge-fornull

while [[ `ps -aef | grep instagram-merge.pl | grep -v grep | wc -l` -gt 0 ]]
do
	sleep 5
done

# Get the latest count of with and without Null from the respective Week tables
echo "--- `date` $line post counts";
echo "select count(*) as null_counts from instagram.entity_fan_ALL_master where last_act_time is NULL " | mysql -t -uinstagram -hstage5 -pinstagrampass -A
echo "select count(*) as non_null_counts from instagram.entity_fan_ALL_master where last_act_time is NOT NULL " | mysql -t -uinstagram -hstage5 -pinstagrampass -A

done >> all-merge.log 2>&1

