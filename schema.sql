CREATE DATABASE IF NOT EXISTS `instagram_crawl`;
GRANT ALL PRIVILEGES ON instagram_crawl.* to instagram@localhost identified by 'instagrampass';
GRANT ALL PRIVILEGES ON instagram_crawl.* to instagram@'127.0.0.1' identified by 'instagrampass';
FLUSH PRIVILEGES;
USE `instagram_crawl`;

DROP TABLE IF EXISTS `entity_fan`;
CREATE TABLE `entity_fan` (
  `post_link` varchar(255) NOT NULL,
  `profile_id` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `userid` varchar(128) NOT NULL,
  `fan_relationship` varchar(64) NOT NULL,
  `message` text DEFAULT NULL,
  `status` ENUM('new', 'crawling', 'crawled', 'merging', 'merged', 'validated', 'crawlerror', 'mergeerror', 'validationerror') NOT NULL DEFAULT 'new',
  `comment_id` bigint(20) DEFAULT NULL,
  `created_time` bigint(20) DEFAULT NULL,
  `fetch_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `polarity` smallint(4) DEFAULT NULL,
  `subjectivity` smallint(4) DEFAULT NULL,
  KEY `comment_id` (`comment_id`),
  KEY `post_link` (`post_link`),
  KEY `profile_id` (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `entity_fan_extended`;
CREATE TABLE `entity_fan_extended` (
      `category` char(3) NOT NULL,
      `profile_id` varchar(128) NOT NULL,
      `fan_id` varchar(255) DEFAULT NULL,
      `fan_relationship` int(10) unsigned NOT NULL,
      `num_times` int(10) unsigned DEFAULT '1',
      UNIQUE KEY `profile_id` (`profile_id`,`fan_id`,`fan_relationship`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `job_entities`;
CREATE TABLE `job_entities` (
  `aa_id` int(10) unsigned NOT NULL,
  `category` varchar(3) NOT NULL,
  `profile_id` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `catalog_week` DATE NOT NULL,
  `num_http_reqs` int(10) unsigned DEFAULT '0',
  `num_posts` int(10) unsigned DEFAULT '0',
  `num_users` int(10) unsigned DEFAULT '0',
  `num_comments` int(10) unsigned DEFAULT '0',
  `num_likes` int(11) DEFAULT '0',
  `vid_comments` int(11) DEFAULT '0',
  `vid_likes` int(11) DEFAULT '0',
  `global_posts` int(11) DEFAULT '0',
  `global_followers` int(11) DEFAULT '0',
  `global_following` int(11) DEFAULT '0',
  `status` ENUM('new', 'crawling', 'crawled', 'merging', 'merged', 'validated', 'crawlerror', 'mergeerror', 'validationerror') NOT NULL DEFAULT 'new',
  `max_post_age` INTEGER UNSIGNED NOT NULL DEFAULT '864000',
  `crawl_code` varchar(32) DEFAULT NULL,
  `crawl_hostname` varchar(16) DEFAULT NULL,
  `crawl_start_time` datetime DEFAULT NULL,
  `crawl_end_time` datetime DEFAULT NULL,
  `crawl_pid` int(11) DEFAULT NULL,
  `merge_hostname` varchar(16) DEFAULT NULL,
  `merge_start_time` datetime DEFAULT NULL,
  `merge_end_time` datetime DEFAULT NULL,
  `merge_pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`aa_id`),
  UNIQUE KEY `profile_id` (`profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `relationship_map`;
CREATE TABLE `relationship_map` (
  `id` int(10) unsigned DEFAULT NULL,
  `rel` varchar(128) NOT NULL,
  `is_binary` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `relationship_map` WRITE;
INSERT INTO `relationship_map` VALUES (1,'instagram_commenters','Y'),(2,'instagram_likers','Y'),(4,'instagram_video_commenters','Y'),(8,'instagram_video_likers','Y'),(16,'instagram_content_liker','Y'),(32,'instagram_frequent_content_liker','Y'),(64,'instagram_content_commenter','Y'),(128,'instagram_frequent_content_commenter','Y');
UNLOCK TABLES;


CREATE DATABASE IF NOT EXISTS `instagram_universe`;
GRANT ALL PRIVILEGES ON instagram_universe.* to instagram@localhost identified by 'instagrampass';
GRANT ALL PRIVILEGES ON instagram_universe.* to instagram@'127.0.0.1' identified by 'instagrampass';
FLUSH PRIVILEGES;

USE `instagram_universe`;

DROP TABLE IF EXISTS `fan_universe`;
CREATE TABLE `fan_universe` (
  `aa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nw_id` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `num_posts` int(10) unsigned DEFAULT '0',
  `num_followers` int(10) unsigned DEFAULT '0',
  `num_following` int(10) unsigned DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`aa_id`),
  UNIQUE KEY `nw_id` (`nw_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `aa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` varchar(64) NOT NULL,
  `post_id` varchar(64) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(4096) DEFAULT NULL,
  `num_likes` int(10) unsigned DEFAULT '0',
  `num_comments` int(10) unsigned DEFAULT '0',
  `created_time` varchar(8) DEFAULT NULL,
  `fetch_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`aa_id`),
  UNIQUE KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `postcomment`;
CREATE TABLE `postcomment` (
  `aa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_aaid` int(10) unsigned DEFAULT NULL,
  `post_aaid` int(10) unsigned DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4,
  `comment_id` bigint(20) DEFAULT NULL,
  `created_time` bigint(20) DEFAULT NULL,
  `fetch_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`aa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

