#!/bin/bash

if [ -z $1 ];then
    echo "syntax: $0 <2014aug27>"
    exit
fi

catalog=$1

grep ^db hosts.txt | while read line; do echo $line | NODE_ENV=new-db-node ./setup-ec2.sh $catalog $(awk '/db/ {print $3, $2}') root; done

