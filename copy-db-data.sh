#!/bin/bash


if [ -z $1 ];then
    echo "syntax: $0 <catalogweek>";
    exit
fi

catweek=$1

# Program do download the database files from remote EC2 nodes
echo "Started downloading data from remote EC2 server - $(date)"
for host in `awk '/db/ {print $3}' hosts.txt`
do
    echo Copying from $host
    mkdir /mnt/gonguradrive/instagram/$catweek

    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem root@$host:/disk/xvdg/completed-backup* /mnt/gonguradrive/instagram/$catweek
done

