#!/bin/bash


if [ -z $1 ];then
    echo "syntax: $0 <catalogweek>";
    exit
fi

catweek=$1

# Program do download the database files from remote EC2 nodes
#echo "Started downloading data from remote EC2 server - $(date)"

#for host in `awk '/db/ {print $3}' hosts.txt`
#do
    #echo Copying from $host
    mkdir /mnt/post-scripts/$catweek

    #scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem root@$host:/disk/xvdg/completed-backup* /mnt/post-scripts/$catweek
#done

# Print details
#echo "Completed. All backups are present under $(pwd)/completed-backups/ directory";
#ls -l /mnt/post-scripts/$catweek/completed-backups/

echo " Started Downloading the jsonData from Crawl Servers - $(date)"
for host in `awk '/crawl/ {print $3}' hosts.txt`
do
   crawlserver=`grep $host hosts.txt | cut -d " " -f 1` ; 
   echo Copying the Crawl json files from $host : $crawlserver 
   mkdir -p /mnt/post-scripts/$catweek/$crawlserver
   scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[0-9]*.json /mnt/post-scripts/$catweek/$crawlserver
   scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[a-n]*.json /mnt/post-scripts/$catweek/$crawlserver
   scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[o-z]*.json /mnt/post-scripts/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[A-N]*.json /mnt/post-scripts/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[O-Z]*.json /mnt/post-scripts/$catweek/$crawlserver
   scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[0-9].log /mnt/post-scripts/$catweek/$crawlserver

    destfilename=`echo $catweek"_"$crawlserver`
    tar -cvzf $destfilename /mnt/post-scripts/$catweek/$crawlserver
    #s3cmd put $destfilename  s3://affinityanswers/instagram/
    s3cmd put $destfilename  s3://affinityanswers-backups/instagram/jsonFiles/
    rm $destfilename

done

echo " Completed the Downloading Crawl Data - $(date) "

# print status
echo "All completed - $(date)."
