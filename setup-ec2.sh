#!/bin/bash

if [ -z $NODE_ENV ];then
    echo "NODE_ENV variable should be exported.";
    echo "Supported values are for environment variable"
    echo " - deploy                     <pub-ip> <priv-ip>"
    echo " - db                         <pub-ip> <priv-ip>"
    echo " - crawl                      <pub-ip> <priv-ip>"
    echo " - new-crawl-node-db-access   <pub-ip> <priv-ip>"
    echo " -"
    echo " - new-crawl-node             <pub-ip> <priv-ip>"
    echo " - new-db-node                <pub-ip> <priv-ip>"
    exit 1;
fi

if [ -z $1 ];then
    echo "syntax: $0 [catalogweek] [Public-IP-Address] [Private-IP-Address] <username>"
    exit 1;
fi

if [ -z $2 ];then
    echo "syntax: $0 [catalogweek] [Public-IP-Address] [Private-IP-Address] <username>"
    exit 1;
fi

if [ -z $3 ];then
    echo "syntax: $0 [catalogweek] [Public-IP-Address] [Private-IP-Address] <username>"
    exit 1;
fi

CATALOG=$1
PUB_IP=$2
PRIV_IP=$3
USER=centos

if [[ ! -z $4 ]];then
    USER=$4
fi

echo "Connecting as : $USER"

# If code is deployed to remote server
if [ $NODE_ENV == "deploy" ];then
    echo -n "Copying the source code .... : "
    scp -o 'StrictHostKeyChecking=no' -c arcfour -C -i ~/.ssh/affinityanswers.pem \
        _screenrc hosts.txt \
        my.cnf \
        start-crawl.sh \
        reset-crawl-states.sh \
        Entity_Catalog.txt schema.sql \
        instagram-crawler.* Instagram* \
        backup-database-from-ec2-server.sh \
        setup-ec2.sh import-instagram-catalog.sh $USER@$PUB_IP:~ > $PUB_IP.copy.log 2>&1
    if [ $? -ne 0 ];then
        echo "[failure]"
        exit
    else
        echo "[success]"
    fi
fi

# If its a singleton database node
if [ $NODE_ENV == "db" ];then
    echo -n "-----> Setting up for db node (logfile is setup.log) : "
    {
        # Setup the hostname
        echo "Trying to find current IP Address ...."
        MYIP=$(/sbin/ifconfig eth0 | grep inet | head -1 | awk '{print $2}' | cut -d: -f2)
        echo "Found my IP Address as $MYIP";
        if [ ! -z $MYIP ];then
            MYNAME=$(grep $MYIP $HOME/hosts.txt | awk '{print $1}')
            if [ ! -z $MYNAME ];then
                echo "Setting hostname to $MYNAME"
                sudo hostname $MYNAME
            fi
        fi
        # Setup other environment related settings
        cp _screenrc ~/.screenrc
        rm -f /etc/localtime && ln -sf /usr/share/zoneinfo/US/Pacific /etc/localtime
        yum update -y
        yum remove mysql mysql-* -y
        rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
	 sed -i.orig -e 's/https/http/' /etc/yum.repos.d/epel.repo

        rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
        yum --enablerepo=remi,remi-test install mysql mysql-server -y
        yum install screen crontabs cronie perl-DBD-MySQL postfix xfsprogs \
                    htop iotop iftop pigz pbzip2 redis -y
        cp /etc/redis.conf /etc/redis.conf.orig
        sed -i.bak -E -e 's/^bind/# bind/' /etc/redis.conf
        chkconfig --levels 235 mysqld on
        chkconfig --levels 235 redis on
        service mysqld start
        service redis start
        mysql_upgrade -u root
        cat ./schema.sql | mysql -u root
        ./import-instagram-catalog.sh $CATALOG ./Entity_Catalog.txt
        mysql -uroot instagram_crawl -e'UPDATE JOB_ENTITIES set max_post_age = 86400 * 30 where max_post_age > 0'
        for HOST in `awk '/crawl/ {print $2}' hosts.txt`
        do
            NODE_ENV=new-crawl-node-db-access ./setup-ec2.sh $CATALOG 127.0.0.1 $HOST root
        done
        sudo service iptables stop && sudo chkconfig iptables off
        echo 'export MYSQL_PS1="\u@\h [\d]> "' >> ~/.bashrc
        source ~/.bashrc
        for disk in xvdf xvdg
        do
            mkdir -p /disk/$disk && mkfs.xfs -f /dev/$disk && mount /dev/$disk /disk/$disk
            if [[ `grep -c $disk < /etc/fstab` -eq 0 ]];then
                echo "/dev/$disk /disk/$disk xfs defaults,noatime,nodiratime,nobarrier 0 0" >> /etc/fstab
            fi
        done
        cat /etc/fstab
        mount -a
        df -h
        echo "=== Disable Selinux & Reboot now ... =="
        if [[ ! -f /etc/selinux/config.new ]];then
            cat /etc/selinux/config | sed -E -e 's/SELINUX=enforcing/SELINUX=disabled/' > /etc/selinux/config.new
            mv /etc/selinux/config /etc/selinux/config.orig
            mv /etc/selinux/config.new /etc/selinux/config
        fi
        cat /etc/selinux/config
        echo "=== setting up mysql ==="
        service mysqld stop && if [[ ! -f /etc/my.cnf.orig ]];then mv /etc/my.cnf /etc/my.cnf.orig; fi && cp my.cnf /etc/my.cnf
        service mysqld start
        {
            echo "alter table instagram_crawl.entity_fan engine=innodb;"
            echo "alter table instagram_crawl.job_entities engine=innodb;"
            echo "alter table instagram_crawl.job_entities_extended engine=innodb;"
            echo "alter table instagram_crawl.relationship_map engine=innodb;"
            echo "alter table instagram_universe.fan_universe engine=innodb;"
        } | mysql -u root
        echo "=== moving innodb spaces ==="
        service mysqld stop
        mv /var/lib/mysql /disk/xvdf && chown -R mysql:mysql /disk/xvdf/mysql && cd /var/lib && ln -s /disk/xvdf/mysql
    } > setup.log 2>&1
    echo "[completed]"
    echo "== all done. rebooting in 10 secs=="
    sleep 10
    shutdown -r now
fi

# If its a crawl node
if [ $NODE_ENV == "crawl" ];then
    echo "----> setting up for crawl node (logfile is setup.log) ";
    {
        # Setup the hostname
        echo "Trying to find current IP Address ...."
        MYIP=$(/sbin/ifconfig eth0 | grep inet | head -1 | awk '{print $2}' | cut -d: -f2)
        echo "Found my IP Address as $MYIP";
        if [ ! -z $MYIP ];then
            MYNAME=$(grep $MYIP ./hosts.txt | awk '{print $1}')
            if [ ! -z $MYNAME ];then
                echo "Setting hostname to $MYNAME"
                sudo hostname $MYNAME
            fi
        fi
	# disable ipv6
	echo 1 | sudo tee /proc/sys/net/ipv6/conf/all/disable_ipv6
	echo 1 | sudo tee /proc/sys/net/ipv6/conf/default/disable_ipv6
        cp _screenrc ~/.screenrc
        sudo rm -f /etc/localtime && sudo ln -sf /usr/share/zoneinfo/US/Pacific /etc/localtime
        sudo yum update -y

        sudo yum install epel-release -y
        sudo yum install 'perl(FindBin)' \
            'perl(File::Spec)' 'perl(JSON)' 'perl(Data::Dumper)' 'perl(Cwd)' \
            'perl(DBI)' 'perl(DBD::mysql)' 'perl(File::Slurp)' \
            'perl(LWP::UserAgent)' 'perl(Time::HiRes)' \
            'perl(LWP::Protocol::https)' \
            'perl(Redis)' \
            htop iotop iftop \
            mysql screen -y
        LAN_IP=$(awk '/db/ {print $2}' hosts.txt | tail -1)
        # Setup the db server ip address
        if [ ! -f Instagram.json.orig ];then
            cp Instagram.json Instagram.json.orig
        fi
        cat Instagram.json.orig | sed -E -e "s/localhost|stage5/$LAN_IP/g" > Instagram.json
    } > setup.log 2>&1
    echo "[complete]"
fi

# Add new crawl node to database server
if [ $NODE_ENV == "new-crawl-node-db-access" ];then
    echo "Giving remote database access to $PRIV_IP"
    {
        echo "GRANT ALL PRIVILEGES ON instagram_crawl.* to instagram@$PRIV_IP identified by 'instagrampass';";
        echo "GRANT ALL PRIVILEGES ON instagram_universe.* to instagram@$PRIV_IP identified by 'instagrampass';";
        echo "FLUSH PRIVILEGES;";
    } | mysql -u root
fi

#########################################
# OPTIONS TO BE USED FROM LOCAL DEV BOX #
#########################################

# New Crawl Node setup from local dev box to remote machine
if [ $NODE_ENV == "new-crawl-node" ];then
    echo "Setting up new node with ip $PUB_IP"
    NODE_ENV=deploy ./setup-ec2.sh $CATALOG $PUB_IP $PRIV_IP $USER
    ssh -o 'StrictHostKeyChecking=no' -t -t -i ~/.ssh/affinityanswers.pem $USER@$PUB_IP 'NODE_ENV=crawl ./setup-ec2.sh '$CATALOG' localhost localhost '$USER
fi

# New Crawl Node setup from local dev box to remote machine
if [ $NODE_ENV == "new-db-node" ];then
    echo "Setting up new node with ip $PUB_IP"
    NODE_ENV=deploy ./setup-ec2.sh $CATALOG $PUB_IP $PRIV_IP $USER
    ssh -o 'StrictHostKeyChecking=no' -t -t -i ~/.ssh/affinityanswers.pem $USER@$PUB_IP 'NODE_ENV=db ./setup-ec2.sh '$CATALOG' localhost localhost '$USER
    ssh -o 'StrictHostKeyChecking=no' -t -t -i ~/.ssh/affinityanswers.pem $USER@$PUB_IP 'NODE_ENV=new-crawl-node-db-access ./setup-ec2.sh '$CATALOG' '$PRIV_IP' '$PUB_IP' '$USER
fi
