#!/bin/bash

# Script to take backup of the tables on EC2 instances
COMPLETED=/disk/xvdg/completed-backups
mkdir -p $COMPLETED/
cd /root && ln -s $COMPLETED

# Take ownership of backup directory 
chown -R mysql:mysql /disk/xvdg

# Start the backup
for TABLE in job_entities entity_fan
do
    if [[ ! -f $TABLE.tsv.bz2 || -z $TABLE.tsv.bz2 ]];then
        OUTFILE=$COMPLETED/$TABLE.tsv
        echo "Taking backup of $TABLE"
        mysqldump -u root -d instagram_crawl $TABLE > $COMPLETED/instagram_crawl.$TABLE.sql && \
        echo "select * INTO OUTFILE '$OUTFILE' FROM $TABLE;" | mysql -u root instagram_crawl && \
        pbzip2 $OUTFILE
    fi
    ls -l $COMPLETED/
done

# New fans that got added to the system are available here
# FIXME: How do we merge this with existing user dataset (aa_id is autoincrement
# and will not preserve across crawls, so the fan_universe aa_id is ignorable)
if [ ! -f fan_universe.sql.bz2 ];then
    echo "Taking backup of fan_universe"
    mysqldump -u root --compact --insert-ignore instagram_universe fan_universe | pbzip2 > $COMPLETD/instagram_universe.fan_universe.sql.bz2
fi

# TSV version of the fan_univers which can be appended to existing fan_universe on instagram_universe table
OUTFILE=$COMPLETED/fan_universe.tsv
echo "Taking backup of fan_universe into $OUTFILE"
echo "select nw_id, username, profile_picture, full_name, num_posts, num_followers, num_following, createdAt, updatedAt INTO OUTFILE '$OUTFILE' FROM fan_universe;" | mysql -u root instagram_universe
pbzip2 $OUTFILE


if [ ! -f post.tsv.bz2 ];then
    OUTFILE=$COMPLETED/post.tsv
    echo "Taking backup of post"
    mysqldump -u root -d instagram_universe post > $COMPLETED/instagram_universe.post.sql && \
    echo "select * INTO OUTFILE '$OUTFILE' FROM post;" | mysql -u root instagram_universe && \
    pbzip2 $OUTFILE
    ls -l $COMPLETED/
fi

echo $COMPLETED
ls -l $COMPLETED/

echo "All completed ..."
