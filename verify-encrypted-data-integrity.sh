#!/bin/bash

# Check all the entity_fan tables for any corruption
echo "Checking entity_fan table for any corruption ...";
mysql -t -uroot -A -hlocalhost instagram_crawl -e'set @aa_key = "63306C6C6967656E7421416666696E697479416E7377657273"; select count(*) from entity_fan where hex(aes_encrypt(userid, unhex(@aa_key))) is NULL or hex(aes_encrypt(username, unhex(@aa_key))) is NULL'

# Script to make sure that all encrypted data is decryptable

#echo "Checking UUID_ig table for any corruption ..."
#mysql -t -uinstagram -pinstagrampass -A -hlocalhost all_networks -e'set @aa_key = "63306C6C6967656E7421416666696E697479416E7377657273";select count(*) from UUID_ig where aes_decrypt(unhex(fan_id), unhex(@aa_key)) is NULL'

# Check all the master tables
#for C in ART BRN CEL DIG GAM LOC MAG MOV RAD SPR TVS
#do
#    table=entity_fan_${C}_master
#    echo "Checking $table for any corruption .."
#    mysql -t -uinstagram -pinstagrampass -A -hlocalhost instagram -e'set @aa_key = "63306C6C6967656E7421416666696E697479416E7377657273"; select count(*) from '$table' where aes_decrypt(unhex(fan_id), unhex(@aa_key)) is NULL'
#done

