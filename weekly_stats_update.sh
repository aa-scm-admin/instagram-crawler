#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage : $0 <YYYY-MM-DD>"
    exit 1
fi

catweek="$1"
dt=$(date +%Y%b%d -d "$catweek"|tr '[:upper:]' '[:lower:]')

echo $dt

mysql -u root -h localhost -D instagram_statistics -e "INSERT INTO weekly_statistics (catalog_week, entity_id, ig_id, num_posts, num_comments, num_likes, uniq_fans) SELECT '$catweek', je.aa_id, je.profile_id, je.num_posts, (je.num_comments+je.vid_comments), (je.num_likes+je.vid_likes), count(distinct ef.userid_hash) from instagram_crawl.job_entities_$dt je LEFT JOIN instagram_crawl.entity_fan_$dt ef ON je.profile_id = ef.profile_id group by je.profile_id;"

mysqldump -uroot instagram_statistics weekly_statistics --skip-add-drop-table --no-create-info --insert-ignore --no-autocommit --where="catalog_week='$catweek'" | ssh -i ~/.ssh/affinityanswers.pem centos@gongura-backend1.affinityanswers.com 'mysql -uroot -pindie instagram_statistics'
