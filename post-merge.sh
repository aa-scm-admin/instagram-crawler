#!/bin/bash

# Author: Naresh <nareshv@affinityanswers.com>
# Date  : Jul-2014

# After merge is complete, run this script to count the fans
#
# instagram@stage5 [instagram]> insert into common_profile_counts select count(distinct entity_uid) total_profiles, uuid fan_uuid from entity_fan_ALL_master group by uuid order by total_profiles desc;
# Query OK, 17147856 rows affected (3 min 19.45 sec)
# Records: 17147856  Duplicates: 0  Warnings: 0
# 
# instagram@stage5 [instagram]> select * from common_profile_counts limit 5;
# +----------------+----------+
# | total_profiles | fan_uuid |
# +----------------+----------+
# |            451 |    66914 |
# |            432 |    59332 |
# |            422 |   217510 |
# |            382 |   145088 |
# |            344 |    98480 |
# +----------------+----------+
# 5 rows in set (0.00 sec)

./optimize-instagram-tables.sh

{
    echo 'truncate table instagram.common_profile_counts;'
    echo 'insert into instagram.common_profile_counts select count(distinct entity_uid) total_profiles, uuid fan_uuid from instagram.entity_fan_ALL_master group by uuid order by total_profiles desc;'
    echo 'select count(*) from instagram.common_profile_counts;'
    echo 'truncate table instagram.common_fan_counts;'
    echo 'insert into instagram.common_fan_counts select count(distinct uuid) total_fans, entity_uid from entity_fan_ALL_master group by entity_uid order by total_fans desc;'
    echo 'select count(*) from instagram.common_fan_counts;'
} | mysql -uinstagram -hstage5 -pinstagrampass -A instagram -t

./optimize-instagram-tables.sh
./print-table-counts.sh
