#!/bin/bash

OUTDIR=/mnt/gonguradrive/instagram
while read TT
do
   echo "Exporting the table $TT "
   T=$(echo $TT | sed -e 's/entity_fan_//g')

    echo "Checking : $OUTDIR/$T.sentisql.gz"
    if [[ ! -e "$OUTDIR/$T.sentisql.gz" ]];then
        #SEL="select B.aa_id as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '|', 1), 'A1-', '') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 2), '|', -1), 'A2-', '') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 3), '|', -1), 'A3-', '') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like 'http:%' then  REPLACE(REPLACE(post_link, 'http://instagram.com/p/', ''), '/', '') when post_link like 'https:%' then  REPLACE(REPLACE(post_link, 'https://instagram.com/p/', ''), '/', '') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, /* extract_or_create_uuid(A.userid) as uuid */ 0 as uuid , ifnull(A.polarity,'') as polarity, ifnull(A.subjectivity,'') as subjectivity from entity_fan_$T A INNER JOIN job_entities_$T B ON A.profile_id = B.profile_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON B.aa_id = E.entity_uid"
        SEL="select B.aa_id as profile_id, '0' as A1, '0' as A2, '0' A3, 0 as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, 'null' as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, /* extract_or_create_uuid(A.userid) as uuid */ 0 as uuid , ifnull(A.polarity,'') as polarity, ifnull(A.subjectivity,'') as subjectivity from entity_fan_$T A INNER JOIN job_entities_$T B ON A.profile_id = B.profile_id INNER JOIN relationship_map D ON D.rel = A.fan_relationship "
        echo "Working on $T:"
        echo "$SEL;"
        echo "$SEL;" | mysql -u -n --quick -uroot -pindie -hlocalhost -A instagram_crawl | gzip -c > $OUTDIR/$T.sentisql.gz && s3cmd put $OUTDIR/$T.sentisql.gz s3://affinityanswers/incremental/instagram/
        if [ $? -eq 0 ];then
            echo "success";
        else
            echo "failure";
        fi
   fi

   echo " Generating SQL "
   {
        INS="copy incremental_missing_senti_ig from 's3://affinityanswers/incremental/instagram/$T.sentisql.gz' credentials  'aws_access_key_id=AKIAIHW4MJPABIICZO7Q;aws_secret_access_key=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' delimiter '\\t' EMPTYASNULL GZIP IGNOREHEADER 1;"
        echo "BEGIN;"
        echo "$INS";
        echo "COMMIT;"
    } > $OUTDIR/$T.senti.sql

   echo "Executing SQL "
    ~/GonguraArchitecture/load-sql.sh $OUTDIR/$T.senti.sql
    if [ $? -ne 0 ];then
        echo "Failed to run the sql to new database from $OUTDIR/$T.senti.sql"
        exit 1
    fi
    
    echo "Done for $TT "

done < table_list


