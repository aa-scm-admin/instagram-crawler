#!bin/sh

echo "Loading the Matched Data into the Table "
mysql -u root -pindie -h localhost -D instagram_crawl -e "drop table if exists cross_comment_compact_$1 ;"
mysql -u root -pindie -h localhost -D instagram_crawl -e "create table cross_comment_compact_$1 (src_entity_uid int, src_comment_id varchar(30), dest_entity_uid int)"
mysql -u root -pindie -h localhost -D instagram_crawl -e "create unique index i_comment ON cross_comment_compact_$1 (src_comment_id , dest_entity_uid );"
#mysql -u root -pindie -h localhost -D instagram_crawl -e "truncate cross_comment_compact_$1 "

while IFS=":" read -r f1 f2
do
    #echo $f1   $f2
    mysql -u root -pindie -h localhost -D instagram_crawl -e "insert ignore into cross_comment_compact_$1 (src_comment_id , dest_entity_uid) values ( '$f1' , $f2 )"
done < matched.out

echo "Get the other fields in compact table "
   mysql -u root -pindie -h localhost  -e " update instagram_crawl.cross_comment_compact_$1 , instagram_crawl.entity_fan_$1 , instagram_crawl.job_entities_$1 set instagram_crawl.cross_comment_compact_$1.src_entity_uid = instagram_crawl.job_entities_$1.aa_id  where instagram_crawl.cross_comment_compact_$1.src_comment_id = instagram_crawl.entity_fan_$1.comment_id AND instagram_crawl.entity_fan_$1.profile_id = instagram_crawl.job_entities_$1.profile_id"

   mysql -u root -pindie -h localhost  -e " delete from instagram_crawl.cross_comment_compact_$1 where dest_entity_uid = src_entity_uid ; "

echo "Explode the compact table for  Commenters for that comments"
   mysql -u root -pindie -h localhost -D instagram_crawl -e "drop table if exists cross_comment_exploded_$1  ; "
   mysql -u root -pindie -h localhost -D instagram_crawl -e "create table if not exists cross_comment_exploded_$1 (src_entity_uid int, dest_entity_uid int , post_link  varchar(255) ,  userid  varchar(128), fan_relationship varchar(100), comment_id  bigint, created_time  int, fetch_time timestamp, polarity smallint, subjectivity smallint ) ; "
   mysql -u root -pindie -h localhost -D instagram_crawl -e "create unique index i_comment_exploded ON cross_comment_exploded_$1 (dest_entity_uid, comment_id, userid, fan_relationship, created_time, polarity, subjectivity);"
#   mysql -u root -pindie -h localhost -D instagram_crawl -e "truncate cross_comment_exploded_$1 "

   mysql -u root -pindie -h localhost -D instagram_crawl -e "insert ignore into cross_comment_exploded_$1 select src_entity_uid,dest_entity_uid, post_link, userid, 'instagram_cross_comment_commenters', comment_id, created_time, fetch_time, polarity, subjectivity from cross_comment_compact_$1 , entity_fan_$1   WHERE cross_comment_compact_$1.src_entity_uid != cross_comment_compact_$1.dest_entity_uid  AND cross_comment_compact_$1.src_comment_id = entity_fan_$1.comment_id AND entity_fan_$1.fan_relationship  like   'instagram%commenters' ;"

 
echo "Completed"
