#!/bin/bash

for host in `awk '/crawl/ {print $3}' hosts.txt`
do
    echo "Logging in to $host"
    ssh -o 'StrictHostKeyChecking=no' centos@$host "$@"
done
