#!/bin/bash

if [ -z $1 ];then
    echo "syntax: $0 <catalogweek>";
    exit
fi


week=$1;

    mysql -u root -h localhost -D instagram_statistics -e "INSERT INTO weekly_statistics (catalog_week, entity_id, ig_id, num_posts, num_comments, num_likes,uniq_fans) SELECT '$week', count(*),sum(num_posts), sum(num_comments + vid_comments), sum(num_likes + vid_likes) from instagram_crawl.job_entities_$week;"
    mysql -u root -h localhost -D instagram_statistics -e "insert into unique_fans_statistics SELECT '$week', profile_id, count(distinct userid) as user_count from instagram_crawl.entity_fan_$week group by profile_id order by user_count desc limit 20;"
    mysql -u root -h localhost -D instagram_statistics -e "insert into engaged_entities_statistics SELECT '$week', profile_id, count(userid) as user_count from instagram_crawl.entity_fan_$week group by profile_id order by user_count desc limit 20;"
