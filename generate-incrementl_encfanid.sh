#!/bin/bash

TABLES=$(echo "select tablename from entity_fan_meta where status = 'merged' and in_new_warehouse <> 'YES'" | mysql -uroot -hlocalhost -A instagram_crawl -N)

OUTDIR=/mnt/post-scripts

echo "truncate incremental_ig ;"; > $OUTDIR/premerge.sql
#ssh -i ~/.ssh/affinityanswers.pem centos@gongura-compute1.affinityanswers.com "bash ~/GonguraArchitecture/load-sql.sh $OUTDIR/premerge.sql"

cat  $OUTDIR/premerge.sql

echo "next"

echo "truncate incremental_ig ;" > $OUTDIR/premerge.sql
cat  $OUTDIR/premerge.sql

$OUTDIR/load-sql.sh $OUTDIR/premerge.sql


if [ $? -ne 0 ];then
   echo "Failed to run the sql to new database from $OUTDIR/premerge.sql"
   exit 1
fi

echo "Executing the Actual script "

{
for TT in $TABLES
do
    T=$(echo $TT | sed -e 's/instagram_crawl.entity_fan_//g')

	# create the temp table 
	
	# Load the records with UUID as 0 and fanID as encrypted HEX 

	# Join with the UUID_enc to get the UUID of the existing

	# Create the new UUID for the New ones and insert them into the UUID_enc

	# Join with the UUID_enc for the New Fans

	# Create the SQL.GZ file from the temp table

	

    echo "Checking : $OUTDIR/$T.sql.gz"
    if [[ ! -e "$OUTDIR/$T.sql.gz" ]];then
        #SEL="select /* SQL_NO_CACHE HIGH_PRIORITY */ B.aa_id as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '|', 1), 'A1-', '') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 2), '|', -1), 'A2-', '') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 3), '|', -1), 'A3-', '') AS UNSIGNED) as A3, C.id as category,aes_decrypt(unhex(A.userid), unhex('63306C6C6967656E7421416666696E697479416E7377657273')) as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like 'http:%' then  REPLACE(REPLACE(post_link, 'http://instagram.com/p/', ''), '/', '') when post_link like 'https:%' then  REPLACE(REPLACE(post_link, 'https://instagram.com/p/', ''), '/', '') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, 0 as uuid , ifnull(A.polarity,'') as polarity, ifnull(A.subjectivity,'') as subjectivity from entity_fan_$T A INNER JOIN job_entities_$T B ON A.profile_id = B.profile_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON B.aa_id = E.entity_uid"
        SEL="select /* SQL_NO_CACHE HIGH_PRIORITY */ B.aa_id as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '|', 1), 'A1-', '') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 2), '|', -1), 'A2-', '') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 3), '|', -1), 'A3-', '') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like 'http:%' then  REPLACE(REPLACE(post_link, 'http://instagram.com/p/', ''), '/', '') when post_link like 'https:%' then  REPLACE(REPLACE(post_link, 'https://instagram.com/p/', ''), '/', '') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, extract_or_create_uuid(A.userid) as uuid , ifnull(A.polarity,'') as polarity, ifnull(A.subjectivity,'') as subjectivity from entity_fan_$T A INNER JOIN job_entities_$T B ON A.profile_id = B.profile_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON B.aa_id = E.entity_uid"
        echo "Working on $T:"
        echo "$SEL;"
        echo "$SEL;" | mysql -u -n --quick -uroot -hlocalhost -A instagram_crawl | gzip -c > $OUTDIR/$T.sql.gz && s3cmd put $OUTDIR/$T.sql.gz s3://affinityanswers/incremental/instagram/
        if [ $? -eq 0 ];then
            echo "success";
        else
            echo "failure";
        fi
    fi
    {
        INS="copy incremental_ig from 's3://affinityanswers/incremental/instagram/$T.sql.gz' credentials  'aws_access_key_id=AKIAIHW4MJPABIICZO7Q;aws_secret_access_key=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' delimiter '\\t' EMPTYASNULL GZIP IGNOREHEADER 1;"
        #INS="copy incremental_ig from '$OUTDIR/$T.sql.gz' credentials  'aws_access_key_id=AKIAIHW4MJPABIICZO7Q;aws_secret_access_key=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' delimiter '\\t' EMPTYASNULL GZIP IGNOREHEADER 1;"
        echo "BEGIN;"
        echo "$INS";
        #echo "update incremental_ig SET uuid = B.uuid from incremental_ig as A JOIN uuid_ig  B ON A.fan_id = B.fan_id WHERE A.uuid is NULL or A.uuid = 0;";
        echo "COMMIT;"
    } > $OUTDIR/$T.sql

    #ssh -i ~/.ssh/affinityanswers.pem centos@gongura-compute1.affinityanswers.com "bash ~/GonguraArchitecture/load-sql.sh /mnt/gonguradrive/runs/instagram/$T.sql"
    $OUTDIR/load-sql.sh $OUTDIR/$T.sql
    if [ $? -ne 0 ];then
        echo "Failed to run the sql to new database from $OUTDIR/$T.sql"
        exit 1
    fi


    echo "UPDATE entity_fan_meta set in_new_warehouse = 'YES' where tablename = '$TT';" | mysql -uroot -hlocalhost -A instagram_crawl
    if [ $? -ne 0 ];then
        echo "Failed to update the status table"
        exit 1
    fi
done
} | tee $OUTDIR/$(date +%Y-%m-%d).log

echo "select count(*) from incremental_ig where create_time = 0;"; > $OUTDIR/postmerge.sql
echo "select count(*) from incremental_ig where systemtime = 0;"; >> $OUTDIR/postmerge.sql
echo "select count(*) from incremental_ig where profile_id = 0;"; >> $OUTDIR/postmerge.sql
echo "select count(*) from incremental_ig where postid is null;"; >> $OUTDIR/postmerge.sql

$OUTDIR/load-sql.sh $OUTDIR/postmerge.sql
if [ $? -ne 0 ];then
   echo "Failed to run the sql to new database from $OUTDIR/postmerge.sql"
   exit 1
fi

