#!/bin/sh

echo "Getting the Instagram Handles from Entity_catalog.txt"
awk -F'\t' '$9  {print $1 "\t@" $9}' /ccdata2/catalog/$1/Entity_Catalog.txt  > InstagramWords.txt

echo "Getting the crawled posts from DB"
#get the posts captions with @ symbol and keep them in posts.txt file 
   #load the excluded entities into a table
   echo "    Loading the Excluded Entities "
   mysql -u root -pindie -h localhost -D instagram_crawl -e "create table if not exists cross_post_excluded_entities_$1 (entity_uid int )"
   echo "    Truncating the excluded table"
   mysql -u root -pindie -h localhost -D instagram_crawl -e "truncate cross_post_excluded_entities_$1"
   echo "    Populating the excluded table"

   while IFS="," read -r f1 f2 f3 f4
   do
      #echo $f1 
      mysql -u root -pindie -h localhost -D instagram_crawl -e "insert into cross_post_excluded_entities_$1 values ($f1);"
   done < Crosspost_Exclusion_Entities.csv

   #Get the posts that are not part of the excluded entities and had title with letter @ into posts.txt
   echo "    Extracting the post_id, title of the included posts with @ symbol "
   #mysql -u root -pindie -h localhost -D instagram_crawl -N -e  " select post_id, title from instagram_universe.post_$1 where aa_id not in (select entity_uid from instagram_crawl.cross_post_excluded_entities_$1 ) AND title like '%@%' " > posts.txt
   mysql -u root -pindie -h localhost -D instagram_crawl -N -e  " select post_id, title from instagram_universe.post_$1 where profile_id not in (select profile_id from job_entities_$1 inner join cross_post_excluded_entities_$1 on job_entities_$1.aa_id = cross_post_excluded_entities_$1.entity_uid ) AND title like '%@%' " > posts.txt


echo "Getting the Matched Instagram Handles with Post titles "
python testmatchV3.py > posts_matched_$1

echo "Loading the Matched Data into the Table "
mysql -u root -pindie -h localhost -D instagram_crawl -e "drop table if exists cross_post_compact_$1 ;"
mysql -u root -pindie -h localhost -D instagram_crawl -e "create table cross_post_compact_$1 (src_entity_uid int, src_post_id varchar(30), src_post_url varchar(100), dest_entity_uid int)"
mysql -u root -pindie -h localhost -D instagram_crawl -e "create unique index i_post ON cross_post_compact_$1 (src_post_id, dest_entity_uid );"

#mysql -u root -pindie -h localhost -D instagram_crawl -e "truncate cross_post_compact_$1 "

while IFS=":" read -r f1 f2
do
    #echo $f1   $f2
    mysql -u root -pindie -h localhost -D instagram_crawl -e "insert ignore into cross_post_compact_$1 (src_post_id, dest_entity_uid) values ( '$f1' , $f2 )"
done < posts_matched_$1

echo "Get the other fields in compact table "
   mysql -u root -pindie -h localhost  -e " update instagram_crawl.cross_post_compact_$1 , instagram_universe.post_$1 , instagram_crawl.job_entities_$1 set instagram_crawl.cross_post_compact_$1.src_entity_uid = instagram_crawl.job_entities_$1.aa_id, instagram_crawl.cross_post_compact_$1.src_post_url = instagram_universe.post_$1.url where instagram_crawl.cross_post_compact_$1.src_post_id = instagram_universe.post_$1.post_id AND instagram_universe.post_$1.profile_id = instagram_crawl.job_entities_$1.profile_id"
   mysql -u root -pindie -h localhost  -e " delete from instagram_crawl.cross_post_compact_$1 where dest_entity_uid = src_entity_uid ; "

echo "Explode the compact table for Likers and Commenters for that Post"
   mysql -u root -pindie -h localhost -D instagram_crawl -e "drop table if exists cross_post_exploded_$1  ; "
   mysql -u root -pindie -h localhost -D instagram_crawl -e "create table cross_post_exploded_$1 (src_entity_uid int, dest_entity_uid int , post_id varchar(30), post_link  varchar(255) ,  userid  varchar(128), fan_relationship varchar(100), comment_id  bigint, created_time  int, fetch_time timestamp, polarity smallint, subjectivity smallint ) ; "
   mysql -u root -pindie -h localhost -D instagram_crawl -e "create unique index i_post_exploded ON cross_post_exploded_$1 (dest_entity_uid, post_id, userid, fan_relationship, comment_id, created_time, polarity, subjectivity);"

#   mysql -u root -pindie -h localhost -D instagram_crawl -e "truncate cross_post_exploded_$1 "

   mysql -u root -pindie -h localhost -D instagram_crawl -e "insert ignore into cross_post_exploded_$1 select src_entity_uid,dest_entity_uid, src_post_id , cross_post_compact_$1.src_post_url, userid, 'instagram_cross_post_commenters', comment_id, created_time, fetch_time, polarity, subjectivity from cross_post_compact_$1 , entity_fan_$1   WHERE cross_post_compact_$1.src_entity_uid != cross_post_compact_$1.dest_entity_uid  AND cross_post_compact_$1.src_post_url = entity_fan_$1.post_link  AND entity_fan_$1.fan_relationship  like   'instagram%commenters' ;"
   mysql -u root -pindie -h localhost -D instagram_crawl -e "insert ignore into cross_post_exploded_$1 select src_entity_uid,dest_entity_uid, src_post_id , cross_post_compact_$1.src_post_url, userid, 'instagram_cross_post_likers', comment_id, created_time, fetch_time, polarity, subjectivity from cross_post_compact_$1 , entity_fan_$1   WHERE cross_post_compact_$1.src_entity_uid != cross_post_compact_$1.dest_entity_uid  AND  cross_post_compact_$1.src_post_url = entity_fan_$1.post_link AND entity_fan_$1.fan_relationship  like   'instagram%likers' ; "

 
echo "Completed"
