#!/bin/bash

if [ -z $1 ];then
    echo "syntax: $0 <catalogweek>";
    exit
fi


week=$1;

OUTDIR=/mnt/gonguradrive/instagram

    {
        STATS="select profile_id , count(*) from events_ig group by profile_id order by profile_id asc ;"
	echo "\o stats_$week;"
	echo "\t"
        echo "$STATS";
	echo "\t"
	echo "\o ;"
    } > $OUTDIR/stats_$week.sql

    ~/GonguraArchitecture/load-sql.sh $OUTDIR/stats_$week.sql
    if [ $? -ne 0 ];then
        echo "Failed to run the sql to new database from $OUTDIR/stats_$week.sql"
        exit 1
    fi

    #cp ~/stats_$week $OUTDIR

    #if [ $? -ne 0 ];then
    #    echo "Failed to copy the sql file";
    #    exit 1
    #fi

	
    sqlcreatetblcmd="create table if not exists week_counts (profileId int, record_count int)"
    mysql -u root -pindie -h localhost -D instagram_crawl -e "$sqlcreatetblcmd;"

    mysql -u root -pindie -h localhost -D instagram_crawl -e "truncate week_counts;"

    loadcmd="load data local infile '$OUTDIR/stats_$week' into table week_counts fields terminated by '|' optionally enclosed by '\"' lines terminated by '\n' ;"
    mysql -u root -pindie -h localhost -D instagram_crawl -e "$loadcmd;"
 
    mysql -hlocalhost -uinstagram -pinstagrampass -A instagram_statistics -e"alter ignore table weekly_profile_fan_counts add column $week integer unsigned default null;";
    
    mysql -hlocalhost -uinstagram -pinstagrampass -A instagram_statistics -e"insert ignore into weekly_profile_fan_counts (profile_uuid, $week) (select profileId, record_count from instagram_crawl.week_counts where profileId != 0 ) on duplicate key update $week = VALUES($week);";


