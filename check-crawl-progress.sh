#!/bin/bash

for host in `awk '/crawl/ {print $3}' hosts.txt`
do
    echo "Checking $host : "
    ssh -o 'StrictHostKeyChecking=no' -i ~/.ssh/affinityanswers.pem fedora@$host 'ps -aef | head -1 && ps -aef | grep instagram-crawler.pl | grep -v grep'
    if [ $? -ne 0 ];then
        echo -e "\t ( ... No crawl processes running ... )"
    fi
done
