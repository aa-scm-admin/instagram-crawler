#!/usr/bin/bash

# checking the directory
# The following scripts needs to done from  /mnt/gonguradrive/instagram

PREVIOUSWEEK=$1
LATESTWEEK=$2

        if [[ -z $1 || -z $2 ]]
        then 
        echo "USAGE : catalog-log-upload-local-test.sh $1 <YYYYmmmDD>"
        echo "Eg: catalog-upload-local-test.sh "2017jul05" "2017jul12" "
            exit
            fi

echo "`date`"
#checking the latest job_entities table status
echo "select count(*) from job_entities"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) from job_entities";
          if [ $? != 0 ]
          then
             echo "latest job_entities status show process failed"
             exit
          else
             fi

#checking the previous week job_entities table status
echo "select count(*) from job_entities_$PREVIOUSWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) from job_entities_$PREVIOUSWEEK";
          if [ $? != 0 ]
          then
             echo "previous week job_entities status show process failed"
             exit
          else
             fi

#checking the previous week entity_fan table status
echo "select count(*) from entity_fan_$PREVIOUSWEEK"
mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) from entity_fan_$PREVIOUSWEEK";
          if [ $? != 0 ]
          then
             echo "previous week entity_fan status show process failed"
             exit
          else
             fi

#checking the latest entity_fan table status
echo "select count(*) from entity_fan"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) from entity_fan";
          if [ $? != 0 ]
          then
             echo "latest entity_fan status show process failed"
             exit
          else
             fi

#rename the entity_fan tables
echo "alter table entity_fan rename entity_fan_$LATESTWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "alter table entity_fan rename entity_fan_$LATESTWEEK";
          if [ $? != 0 ]
          then
             echo "entity_fan table rename process failed"
             exit
          else
             fi

#rename the job_entities  tables
echo "alter table job_entities rename job_entities_$LATESTWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "alter table job_entities rename job_entities_$LATESTWEEK";
          if [ $? != 0 ]
          then
             echo "job_entities table rename process failed"
             exit
          else
             fi

#inserting the entity_fan table into entity_fan_meta table
echo "insert into entity_fan_meta set tablename = 'instagram_crawl.entity_fan_$LATESTWEEK', status = 'merged', in_new_warehouse = 'NO'"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "insert into entity_fan_meta set tablename = 'instagram_crawl.entity_fan_$LATESTWEEK', status = 'merged', in_new_warehouse = 'NO'";
          if [ $? != 0 ]
          then
             echo "entity_fan table insert into entity_fan_meta process failed"
             exit
          else
             echo "entity_fan table insert into entity_fan_meta process has done successfully"
             fi

#checking the events_ig counts from the redshift
echo "select count(*) from events_ig"
#echo "select count(*) from events_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting events_ig from redshit process has failed"
             exit
          else
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting incremental_ig from redshit process has failed"
             exit
          else
             fi

#delete the data from incremental_ig counts from the redshift
echo "delete from incremental_ig"
#echo "delete from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "deleting incremental_ig from redshit process has failed"
             exit
          else
             echo "data has deleted from incremental_ig has done successfully"
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#iginc1=$(echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura')

echo "$iginc1"

          if [ $iginc1 != 0 ]
          then
             echo "data has not deleted from incremental_ig please look into this"
             exit
          else
             fi

#checking the present working directory
echo "pwd"
pwd

#checking the locale information
echo "locale"
locale

#exporting LC_CTYPE into locale
echo "export LC_CTYPE="en_US.UTF-8""
#export LC_CTYPE="en_US.UTF-8"
          if [ $? != 0 ]
          then
             echo "export LC_TYPE into locale process has failed"
             exit
          else
             echo "exported LC_TYPE into locale has done successfully"
             fi

#checking the locale infromation after export
echo "locale"
locale

#Generating the Incremental_ecnfanid's 
echo "sh -x  generate-incrementl_encfanid.sh `date`"
#sh -x  generate-incrementl_encfanid.sh
          if [ $? != 0 ]
          then
             echo "generating incremental_encfanid process has failed `date`"
             exit
          else
             echo "generated incremental_ecnfanid's into incremental_ig has done successfully `date`"
             fi

#checking the latest entity_fan_meta table status
echo "select * from entity_fan_meta where tablename = 'instagram_crawl.entity_fan_$LATESTWEEK'"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select * from entity_fan_meta where tablename = 'instagram_crawl.entity_fan_$LATESTWEEK'";
          if [ $? != 0 ]
          then
             echo "latest entity_fan_meta status show process failed"
             exit
          else
             fi

#checking the previous week entity_fan table status
echo "select count(*) from entity_fan_$LATESTWEEK"
#entfcnt=$(mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) from entity_fan_$LATESTWEEK;")

echo "$entfcnt"

          if [ $? != 0 ]
          then
             echo "latest entity_fan status show process failed"
             exit
          else
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#incig=$(echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura')

echo "$incig"

          if [ $incig != 0 ]
          then
             echo "data has not deleted from incremental_ig please look into this"
             exit
          else
             fi

echo "$entfcnt = $incig"
        if [ $entfcnt == $incig ]
        then
           echo "entity-fan count and incremental_ig counts are same";
        else
           echo "entity-fan count and incremental_ig counts are not same please look into this";
           exit
        fi

#checking the events_ig counts from the redshift
echo "select count(*) from events_ig"
#echo "select count(*) from events_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting events_ig from redshit process has failed"
             exit
          else
             fi

#checking the incremental_ig uuid's are null or not
echo "select count(*) from incremental_ig where uuid = 0 or uuid is NULL or systemtime = 0 or systemtime is NULL or create_time = 0 or create_time is NULL or category = 0 or category is NULL or code1 = 0 or code1 is NULL"
#incuuid=$(mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) from incremental_ig where uuid = 0 or uuid is NULL or systemtime = 0 or systemtime is NULL or create_time = 0 or create_time is NULL or category = 0 or category is NULL or code1 = 0 or code1 is NULL;")

echo "$incuuid"

          if [ $incuuid != 0 ]
          then
             echo "incremental_ig still has uuid != 0 please look into this"
             exit
          else
             fi

#checking the incremental_ig whre postid like HTTP
echo "SELECT count(*) FROM INCREMENTAL_IG WHERE POSTID LIKE 'HTTP:%'"
#inchttp=$(mysql -uroot -pindie -hlocalhost instagram_crawl -e "SELECT count(*) FROM INCREMENTAL_IG WHERE POSTID LIKE 'HTTP:%'")

echo "$inchttp"

          if [ $inchttp != 0 ]
          then
             echo "incremental_ig still has postid like HTTP please look into this"
             exit
          else
             fi

echo "Please insert the incremental_ig data into events_ig manually `date`"

echo "`date`"
