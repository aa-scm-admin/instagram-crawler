#!/bin/sh

if [ $# -ne 1  ] ; then 

	echo " Provide the Catalog Week "
	exit 
fi 

catweek=$1

echo " Started Downloading the jsonData from Crawl Servers - $(date)"
for host in `awk '/crawl/ {print $3}' hosts.txt`
do
    crawlserver=`grep $host hosts.txt | cut -d " " -f 1` ;
    echo Copying the Crawl json files from $host : $crawlserver 
    mkdir -p /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[1-9]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[a-z]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[A-Z]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver

    destfilename=`echo $catweek"_"$crawlserver`
    tar -cvzf $destfilename /mnt/gonguradrive/instagram/$catweek/$crawlserver
    s3cmd put $destfilename  s3://affinityanswers/instagram/
    rm $destfilename

done


