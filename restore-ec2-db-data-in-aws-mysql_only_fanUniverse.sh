#!/bin/bash

if [ -z $1 ];then
    echo "syntax $0 <dir>";
    exit;
fi

if [ ! -d $1 ];then
    echo "ERROR: $1 is not a directory. Please use proper directory.";
    echo "syntax $0 <dir>";
    exit
fi

dir=$1

echo "************** WARNING ************************"

    echo "3. Restoring fan_universe ...";
    echo -e "3.1 Uncompressing ... : \c"
    if [ -f "$dir/fan_universe.tsv" ]
    then
        echo "[skipped]"
    else
        bunzip2 "$dir/fan_universe.tsv.bz2"
        if [ $? -eq 0 ];then
            echo "[completed]"
        else
            echo "[failure]"
            exit 1;
        fi
    fi
    echo -e "3.2 Restoring ... : \c"
    #mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_universe -e"load data local infile '$dir/fan_universe.tsv' replace into table instagram_universe.fan_universe character set utf8mb4;"
    mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_universe -e"load data local infile '$dir/fan_universe.tsv' replace into table instagram_universe.fan_universe character set utf8mb4;"
    if [ $? -eq 0 ];then
        echo "[completed]"
    else
        echo "[failure]"
        exit 1;
    fi
    echo
    echo

    echo "All complete. $(date)"
