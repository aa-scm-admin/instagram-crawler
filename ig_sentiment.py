#!/usr/local/bin/python

from __future__ import print_function
import MySQLdb as mdb
import MySQLdb.cursors
import sys
from pattern.en import sentiment
import logging
import resource
import smtplib
from email.mime.text import MIMEText
from datetime import timedelta
from datetime import datetime 

with open('./ig_score.log', 'w') as f:
	f.truncate()

logging.basicConfig(format='%(asctime)s %(message)s', 
			filename='./ig_score.log', 
			level=logging.INFO)
logging.captureWarnings(True)

DB_HOST = '127.0.0.1'
DB_USER = 'root'	# Has only select and update privileges on twittter_ds.*
DB_PASS = ''
DB = 'instagram_crawl'
STATUS_TABLE = 'entity_fan_meta'
PAGE_SIZE = 100
MAIL_FROM = 'analytics@colligent.com'
MAIL_TO = 'subhashree@affinityanswers.com'	#

# SQL statements
statements = {
	'get_crawled_tables_without_sentiment' : "SELECT tablename FROM %s WHERE \
		sentiment_scored = 'NO' AND status = 'crawled' \
		ORDER BY id",

	'update_sentiment' : "UPDATE %s SET polarity = %d, \
		subjectivity = %d \
		WHERE comment_id = %d",

	'get_next_n_comments' : "SELECT message, comment_id FROM %s \
		WHERE comment_id > 0 AND comment_id IS NOT NULL \
		AND comment_id > %d \
		ORDER BY comment_id \
		LIMIT %d",

	'update_status' : "UPDATE %s SET sentiment_scored = 'YES', \
		sentiment_scored_at = NOW() \
		WHERE tablename = '%s'",

	'eliminated_fan_count' : "select count(distinct userid) from %s \
		WHERE polarity = -1000 \
		AND subjectivity = 1000",

	'eliminated_entity_fan_count' : "select count(distinct \
		profile_id, userid) from %s \
		WHERE polarity = -1000 \
		AND subjectivity = 1000",

	'total_entity_fan_count' : "select count(*) from %s"
}

def handle_exception(e, exit):
	logging.exception("%d: %s", e.args[0], e.args[1])
	send_email("Sentiment::Instagram Exception Handler")
	if exit: sys.exit(1)

def connect_db():
	con = mdb.connect(DB_HOST, DB_USER, DB_PASS, DB)
	return con

def connect_db_no_buffer():
	'''Returns connection for un-buffered queries'''
	con = mdb.connect(DB_HOST, DB_USER, DB_PASS, DB, 
		cursorclass = MySQLdb.cursors.SSCursor)
	return con

def close_db(con):
	con.close()

def sentiment_summary(tablename):
	'''
	Logs the number of unique fans eliminated,
	number of entity-fan relations eliminated and
	total number of entity-fan relations.
	'''
	con = connect_db()
	cursor = con.cursor()
	cursor.execute(statements['eliminated_fan_count'] 
		% (tablename))
	rows = cursor.fetchall()
	logging.info("Summary: Unique fans to be eliminated = %d" 
		% (rows[0][0]))
	cursor.execute(statements['eliminated_entity_fan_count'] 
		% (tablename))
	rows = cursor.fetchall()
	logging.info("Summary: Entity-Fan relations to be eliminated = %d" 
		% (rows[0][0]))
	cursor.execute(statements['total_entity_fan_count'] 
		% (tablename))
	rows = cursor.fetchall()
	logging.info("Summary: Total Entity-Fan relations = %d" 
		% (rows[0][0]))
	close_db(con)
	return True 

def get_tables():
	'''
	Returns a tuple with names of tables eligibile for sentiment scoring
	'''
	return sys.argv[1:]

def update_status(table):
	'''
	Sets the 'sentiment_scored' status for the table to 'YES'
	and the time when scoring got over
	'''
	con = connect_db()
	cursor = con.cursor()
	s = cursor.execute(statements['update_status'] % (STATUS_TABLE, table))
	close_db(con)
	return s 


def to_int(float_val):
	'''Converts sentiment float values to int for efficient storage'''
	return float_val * 1000

def try_fn(fn, alt_val):
	'''Wrapper to keep map() conscise.'''
	try:
		return fn()
	except Exception as e: 
		logging.exception("exception : %s", e.message)
		return alt_val

def score(comment):
	'''Score a comment, convert float to int, merge scores with the comment'''
	return try_fn(
		lambda: comment + tuple(map(to_int, sentiment(comment[0]))),
		comment + (None, None)
	)

def update_fn(tablename, con):
	'''Returns a function to update a comment's sentiment scores'''
	def update_table(comment):
		if (comment[2] is None) or (comment[3] is None):
			return comment
		cursor = con.cursor()
		cursor.execute(statements['update_sentiment'] % 
			(tablename, int(comment[2]), int(comment[3]), comment[1]))
		cursor.close()
		return comment

	return update_table

def process_table(table):
	'''
	Pages through comments in a table, scores the comments
	and updates them with the scores.

	Paging is required to keep SELECTs small so that
	they don't block subsequent UPDATEs.

	UPDATEs require a table lock in MyISAM and they won't
	get the lock if there is a huge SELECT in the queue
	before them.

	Don't use MySQL OFFSET and LIMIT for pagination as
	a long scan is required before returning pages
	that are further down from start.
	'''
	tablename = table
	con = connect_db_no_buffer()
	con_update = connect_db()
	last_id = 0
	cursor = con.cursor()
	
	while True:
		cursor.execute(statements['get_next_n_comments'] % 
			(tablename, last_id, PAGE_SIZE))
		comments = cursor.fetchall()
		if len(comments) == 0:
			break;
		last_id = comments[-1][1]

		map(update_fn(tablename, con_update), map(score, comments))

	logging.info("Processed table %s", tablename)
	sentiment_summary(tablename)

	if cursor: cursor.close()
	if con: close_db(con)
	if con_update: close_db(con_update)

def send_email(subject = None):
	'''
	Notify status.
	'''
	fp = open('./ig_score.log', 'r')
	msg = MIMEText(fp.read())
	fp.close()
	if subject:
		msg['subject'] = subject 
	else:
		msg['subject'] =  'Sentiment::Instagram'
	msg['from'] = MAIL_FROM
	msg['to'] = MAIL_TO
	s = smtplib.SMTP('localhost')
	s.sendmail(MAIL_FROM, [MAIL_TO], msg.as_string())
	s.quit()

def main(args):
	try:
		t1 =  datetime.now()
		tables = args 
		logging.info("Start processing %d tables", len(tables))
		map(process_table, tables)
		t2 = datetime.now()
		t = t2 - t1
		logging.info("Took %f hours", t.total_seconds() / (60 * 60))
	except mdb.Error, e:
		handle_exception(e, True)
	except Exception as e: 
		logging.exception("exception : %s", e.message)
		send_email("Sentiment::Instagram Exception Handler")
		sys.exit(1)

	send_email()

if __name__ == "__main__":
	main([] + sys.argv[1:])
