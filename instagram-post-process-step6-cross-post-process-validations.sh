#!/usr/bin/bash

# checking the directory
# The following scripts needs to done from  /home/colligent/instagram-crossposting/ as a colligent user

LATESTWEEK=$1
LASTUPDATETIME=$2
PREVWEEKDATE=$3

        if [[ -z $1 || -z $2 || -z $3 ]]
        then 
        echo "USAGE : catalog-log-upload-local-test.sh $1 <YYYYmmmDD>"
        echo "Eg: catalog-upload-local-test.sh "2017jul05" '2017-07-12' '2017-07-05'"
            exit
            fi

#rename the posts tables
echo "alter table instagram_universe.post rename to instagram_universe.post_$LATESTWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "alter table instagram_universe.post rename to instagram_universe.post_$LATESTWEEK";
          if [ $? != 0 ]
          then
             echo "posts table rename process failed"
             exit
          else
             fi

#moving to the /home/colligent/instagram-crossposting/ directory
#cd /home/colligent/instagram-crossposting/
        if [ $? != 0 ]
        then
           echo "directory has not changed - Operation Failed "
           exit 1
        else
           echo "directory has changed to /home/colligent/instagram-crossposting/ successfully "
        fi

#running get cross-post-users from DB
echo "sh getcrossusers.sh $LATESTWEEK `date`"
#sh getcrossusers.sh $LATESTWEEK
          if [ $? != 0 ]
          then
             echo "get crosspostusers running process has failed `date`"
             exit
          else
             echo "crosspostusers process has done successfully `date`"
             fi

#checking the count(*) of cross_comment_exploded_* table status
echo "select count(*) from cross_comment_exploded_$LATESTWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select count(*) from cross_comment_exploded_$LATESTWEEK";
          if [ $? != 0 ]
          then
             echo "cross_comment_exploded_$LATESTWEEK status show process failed please look into this"
             exit
          else
             fi

#checking the count(*) of cross_comment_exploded_* where uuid is null table status
echo "select count(*) from cross_comment_exploded_$LATESTWEEK where src_entity_uid is null OR dest_entity_uid is NULL OR userid is null OR fan_relationship is null OR comment_id is null OR created_time is NULL"
#ccecnt=$(mysql -uroot -pindie -hlocalhost instagram_crawl -A "select count(*) from cross_comment_exploded_$LATESTWEEK where src_entity_uid is null OR dest_entity_uid is NULL OR userid is null OR fan_relationship is null OR comment_id is null OR created_time is NULL;")
          if [ $ccecnt != 0 ]
          then
             echo "cross_comment_exploded_$LATESTWEEK where uuid is null != 0 please look into this"
             exit
          else
             fi

#checking the count(*) of cross_comment_exploded_* where polarity and subjextivity is null table status
echo "select count(*) from cross_comment_exploded_$LATESTWEEK where polarity is NULL or subjectivity is NULL"
#ccepscnt=$(mysql -uroot -pindie -hlocalhost instagram_crawl -A "select count(*) from cross_comment_exploded_$LATESTWEEK where polarity is NULL or subjectivity is NULL;")
          if [ $ccepscnt != 0 ]
          then
             echo "cross_comment_exploded_$LATESTWEEK where polariy and subjectivity is null != 0 please look into this"
             exit
          else
             fi

#checking the min/max (fetch_time) from cross_comment_exploded_$LATESTWEEK
echo "select min(fetch_time), max(fetch_time) from cross_comment_exploded_$LATESTWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select min(fetch_time), max(fetch_time) from cross_comment_exploded_$LATESTWEEK";
          if [ $? != 0 ]
          then
             echo "getting min/max (fetch_time) from cross_comment_exploded_$LATESTWEEK show process failed please look into this"
             exit
          else
             fi

#checking the min/max (fetch_time) from cross_comment_exploded_all
echo "select min(fetch_time), max(fetch_time) from cross_comment_exploded_all"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select min(fetch_time), max(fetch_time) from cross_comment_exploded_all";
          if [ $? != 0 ]
          then
             echo "getting min/max (fetch_time) from cross_comment_exploded_all show process failed please look into this"
             exit
          else
             fi

#rename the cross_comment_exploded_$LATESTWEEK tables
echo "alter table cross_comment_exploded_$LATESTWEEK rename to cross_comment_exploded_$LATESTWEEK_backup_before_dedup"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "alter table cross_comment_exploded_$LATESTWEEK rename to cross_comment_exploded_$LATESTWEEK_backup_before_dedup";
          if [ $? != 0 ]
          then
             echo "cross_comment_exploded_$LATESTWEEK table rename process failed please look int this"
             exit
          else
             fi

#inserting the data into cross_comment_exploded_all from the cross_comment_exploded_$LATESTWEEK_backup* tables
echo "insert  ignore into cross_comment_exploded_all select * from   cross_comment_exploded_$LATESTWEEK_backup_before_dedup"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "insert  ignore into cross_comment_exploded_all select * from   cross_comment_exploded_$LATESTWEEK_backup_before_dedup";
          if [ $? != 0 ]
          then
             echo "data has not incerted into cross_comment_exploded_all table please look int this"
             exit
          else
             echo "data has incerted into cross_comment_exploded_all table successfully"
             fi

#creating the cross_comment_exploded_$LATESTWEEK as cross_comment_exploded_all where fetch_time >= 
echo "create table cross_comment_exploded_$LATESTWEEK  as select * from cross_comment_exploded_all  where fetch_time >= ( select min(fetch_time) from cross_comment_exploded_$LATESTWEEK_backup_before_dedup )"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "create table cross_comment_exploded_$LATESTWEEK  as select * from cross_comment_exploded_all  where fetch_time >= ( select min(fetch_time) from cross_comment_exploded_$LATESTWEEK_backup_before_dedup)";
          if [ $? != 0 ]
          then
             echo "cross_comment_exploded_$LATESTWEEK with fetch_time >= table not created please look int this"
             exit
          else
             echo "cross_comment_exploded_$LATESTWEEK with fetch_time >= table created successfully"
             fi

#inserting the cross_comment_exploded_$LATESTWEEK table into entity_fan_meta table
echo "insert into entity_fan_meta ( tablename, status, in_new_warehouse ) values ('cross_comment_exploded_$LATESTWEEK' , 'merged' , 'NO' )"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "insert into entity_fan_meta ( tablename, status, in_new_warehouse ) values ('cross_comment_exploded_2016dec21' , 'merged' , 'NO' )";
          if [ $? != 0 ]
          then
             echo "cross_comment_exploded_$LATESTWEEK table insert into entity_fan_meta process failed"
             exit
          else
             echo "inserted cross_comment_exploded_$LATESTWEEK table into entity_fan_meta successfully"
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting incremental_ig from redshit process has failed"
             exit
          else
             fi

#delete the data from incremental_ig counts from the redshift
echo "delete from incremental_ig"
#echo "delete from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "deleting incremental_ig from redshit process has failed"
             exit
          else
             echo "data has deleted from incremental_ig has done successfully"
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#iginc1=$(echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura')

echo "$iginc1"

          if [ $iginc1 != 0 ]
          then
             echo "data has not deleted from incremental_ig please look into this"
             exit
          else
             fi

#exporting LC_CTYPE into locale
echo "export LC_CTYPE="en_US.UTF-8""
#export LC_CTYPE="en_US.UTF-8"
          if [ $? != 0 ]
          then
             echo "export LC_TYPE into locale process has failed"
             exit
          else
             echo "exported LC_TYPE into locale has done successfully"
             fi

#Generating the Incremental_ecnfanid's 
echo "sh generate-incrementl_encfanid-crosspost.sh `date`"
#sh generate-incrementl_encfanid-crosspost.sh
          if [ $? != 0 ]
          then
             echo "generating incremental_encfanid-crosspost process has failed `date`"
             exit
          else
             echo "generated incremental_ecnfanid-crosspost into incremental_ig has done successfully `date`"
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#incig=$(echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura')

echo "$incig"

          if [ $incig = 0 ]
          then
             echo "data has not incerted into incremental_ig please look into this"
             exit
          else
             fi

#checking the incremental_ig min (systemtime)
echo "select min(systemtime) from incremental_ig"
#incminsytm=$(echo "select min(systemtime) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura')

echo "$incminsytm"

          if [ $? != 0 ]
          then
             echo "selecting min(systemtime) from incremental_ig process failed please look into this"
             exit
          else
             fi

#checking the incremental_ig max (systemtime)
echo "select max(systemtime) from incremental_ig"
#incmaxsytm=$(echo "select min(systemtime) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura')

echo "$incmaxsytm"

          if [ $? != 0 ]
          then
             echo "selecting max(systemtime) from incremental_ig process failed please look into this"
             exit
          else
             fi

#checking the fan_relationship count from incremental_ig 
echo "select fan_relationship , count(*) from incremental_ig group by fan_relationship"
#echo "select fan_relationship , count(*) from incremental_ig group by fan_relationship" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'

          if [ $? = 0 ]
          then
             echo "getting fan_relationship count from incremental_ig show process failed"
             exit
          else
             fi

#checking the fan_relationship(16,32,64) count from incremental_ig 
echo "select profile_id , count(*) from incremental_ig where fan_relationship in (16,32,64) group by profile_id order by count(*) desc limit 5"
#echo "select profile_id , count(*) from incremental_ig where fan_relationship in (16,32,64) group by profile_id order by count(*) desc limit 5" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'

          if [ $? = 0 ]
          then
             echo "getting fan_relationship(16,32,34) count from incremental_ig show process failed"
             exit
          else
             fi

#inserting data into events_ig
echo "insert into events_ig select * from incremental_ig"
#echo "insert into events_ig select * from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'

          if [ $? = 0 ]
          then
             echo "data insertion into events_ig process has failed"
             exit
          else
             echo "data has inserted successfully into events_ig"
             fi

#checking the events_ig counts from the redshift
echo "select count(*) from events_ig"
#echo "select count(*) from events_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting events_ig from redshit process has failed"
             exit
          else
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting incremental_ig from redshit process has failed"
             exit
          else
             fi

#checking the count(*) from UUID_ig table status
echo "select count(*) from all_networks.UUID_ig"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select count(*) from all_networks.UUID_ig";
          if [ $? != 0 ]
          then
             echo "count(*) from UUID_ig status show process failed please look into this"
             exit
          else
             fi

#checking the max(uuid) from UUID_ig table status
echo "select max(uuid) from all_networks.UUID_ig"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select max(uuid) from all_networks.UUID_ig";
          if [ $? != 0 ]
          then
             echo "max(uuid) from UUID_ig status show process failed please look into this"
             exit
          else
             fi

#checking the count(*) from UUID_ig for last_updt_time table status
echo "select count(*) from all_networks.UUID_ig where last_updt_time > $LASTUPDATETIME"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select count(*) from all_networks.UUID_ig where last_updt_time > `$LASTUPDATETIME`";
          if [ $? != 0 ]
          then
             echo "count(*) from UUID_ig for last_updt_time table show process failed please look into this"
             exit
          else
             fi

#getting the status from ig_weekly_summary for the last week
echo "select * from ig_weekly_summary where cat_week = $PREVWEEKDATE"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select * from ig_weekly_summary where cat_week = `$PREVWEEKDATE`";
          if [ $? != 0 ]
          then
             echo "ig_weekly_summary table status show process failed please look into this"
             exit
          else
             fi

#checking the count(distinct profile_id) from events_ig and fan_relationship in (16,32,64)
echo "select count(distinct profile_id) from events_ig where fan_relationship in (16,32,64)  and systemtime >= $incminsytm and systemtime <= $incmaxsytm"
#echo "select count(distinct profile_id) from events_ig where fan_relationship in (16,32,64)  and systemtime >= $incminsytm and systemtime <= $incmaxsytm | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting incremental_ig from redshit process has failed"
             exit
          else
             fi

echo "`date`"
