#!/bin/bash

# Unified script to do the job of merging the data from EC2 servers

WHOAMI=$(whoami)

if [[ -z $1 || "$1" == "-h" || "$1" == "--help" ]];then
    echo "Syntax: $0 <catalog-week>"
    exit
fi

if [[ "$TERM" != "screen" ]];then
    echo "ERROR: You should run this program from screen session."
    exit;
fi


if [[ "$WHOAMI" != "mobydick" ]];then
    echo "ERROR: You have to run this program as 'mobydick' user"
    exit
fi

# Setup environment variables
BASEDIR=/ccdata2/InstagramLogs/
CATALOG=$1

# 1. Create current catalog week working directory
WDIR=$BASEDIR/$CATALOG
if [ ! -d "$WDIR" ];then
    echo "1. Creating $WDIR"
    mkdir "$WDIR"
else
    echo "1. $WDIR - already exists. Detected double run. Aborting"
    exit
fi

# 2. Switch to current directory
cd "$WDIR" && {
    # 3. Copy the hosts.txt from the Instagram directory
    cp ~/Instagram/hosts.txt ./hosts.txt
    if [ ! -f hosts.txt ];then
        echo "2. Failed to copy ~/Instagram/hosts.txt. Aborting .."
        exit
    fi
    # 4. Take backup of the current week crawl data on remote EC2 Instance
    DBIP=$(awk '/db/ {print $3}' hosts.txt)
    if [ -z $DBIP ];then
        echo "2. Unable to determine IP for database server. Aborting .."
        exit
    fi
    echo "3. Taking backup of database on remote DB $DBIP"
    ssh -i ~/.ssh/affinityanswers.pem root@$DBIP 'cd $HOME && ./backup-database-from-ec2-server.sh' 2>&1 | tee -a backup-database-from-ec2-server.log
    if [ $? -ne 0 ];then
        echo "3. Remote backup failed. Aborting ..."
        exit
    fi
    # 5. Take backup of the current week data on stage5 server
    echo "Taking backup of instagram tables on stage5 server"
    ../stage5-backup-weekly-tables.sh "$CATALOG" "$WDIR" 2>&1 | tee -a stage5-backup-weekly-table.log
    if [ $? -ne 0 ];then
        echo "Local table backup failed. Aborting ..."
        exit
    fi
    # 6. Download the Remote Logs from Crawl Nodes
    ../download-crawl-logs-from-ec2.sh 2>&1 | tee -a download-crawl-logs-from-ec2.log
    if [ $? -ne 0 ];then
        echo "Failed to download crawl logs. Aborting"
        exit
    fi
    # 7. Download the DB backup from EC2
    ../download-db-data-from-ec2.sh 2>&1 | tee -a download-db-data-from-ec2.log
    if [ $? -ne 0 ];then
        echo "Failed to download db data from ec2. Aborting"
        exit
    fi
    # 8. Send email about the current catalog job-entities
    bash ~/Instagram/generate-instagram-crawl-error-report-email.sh $CATALOG
    # 9. Start the merge processes
    MERGEDIR="$CATALOG-merge"
    mkdir "./$MERGEDIR" && cd ~/Instagram && {
        # 9.1 Start the merge processes and wait for them to finish
        ln -s "$WDIR/$MERGEDIR"
        ./start-merge.sh "$MERGEDIR"
        disown -a
        FAIL=0
        for job in `jobs -p`
        do
            echo "Waiting for job $job"
            wait $job || let "FAIL+=1"
        done
        if [ "$FAIL" == "0" ];then
            echo "All merge jobs completed."
        else
            echo "FAIL: Errors in some merge jobs."
            exit
        fi
        # 9.2 Run the merge validator script to make sure we are good to go
    }
} 2>&1 | tee -a full.log
