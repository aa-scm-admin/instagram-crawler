#!/bin/bash

# Desc: Script to start crawl on a given node
# Date: Aug/2014

if [[ -z $ENV_NUM_JOBS ]];then
    NUM_JOBS=2
else
    NUM_JOBS=$ENV_NUM_JOBS
fi
if [[ ! -z $ENV_CRAWL_DIR ]];then
    CRAWL_DIR=$ENV_CRAWL_DIR
else
    CRAWL_DIR=$HOME
fi

echo "[`date`]     : Settings ....."
echo "NUM_JOBS     : $ENV_NUM_JOBS"
echo "CRAWL_DIR    : $CRAWL_DIR"
echo "[`date`]     : Settings ....."

cd $CRAWL_DIR && {
    SCRIPT_PATH="$CRAWL_DIR/instagram-crawler.pl"
    if [ -f "$SCRIPT_PATH" ];then
        echo "[`date`] Number of crawl jobs : $NUM_JOBS"
        for JOB in `seq 1 1 $NUM_JOBS`
        do
            echo "[`date`] Starting Job $JOB"
            perl $SCRIPT_PATH 2>&1 >> ${JOB}.log &
        done
        echo "[`date`] All the jobs are disowned now ...."
        disown -a
    else
        echo "[`date`] Error. $SCRIPT_PATH is not found. Aborting.";
    fi
}

echo "[`date`] All jobs are complete. Please verify below output ..."
ps -f | grep instagram-crawler.pl | grep -v grep
echo "... Good bye !"
