#!/bin/bash

# init
echo "Started downloading logs from crawl servers - $(date)"

# Program do download the logs from remote EC2 nodes
for host in `awk '/crawl/ {print $3}' hosts.txt`
do
    mkdir -p $host
    echo "Downloading Logs from $host (Logs will be saved under $(pwd)/$host/ directory."
    scp -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem fedora@$host:~/*.log $host/
done

# print status
echo "All completed - $(date)."
