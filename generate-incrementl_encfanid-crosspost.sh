#!/bin/bash

TABLES=$(echo "select tablename from entity_fan_meta where status = 'merged' and in_new_warehouse <> 'YES'" | mysql -uroot -pindie -hlocalhost -A instagram_crawl -N)
OUTDIR=/mnt/gonguradrive/instagram

echo "truncate incremental_ig ;"; > $OUTDIR/premerge.sql

cat  $OUTDIR/premerge.sql

echo "next"

echo "truncate incremental_ig ;" > $OUTDIR/premerge.sql
cat  $OUTDIR/premerge.sql

~/GonguraArchitecture/load-sql.sh $OUTDIR/premerge.sql


if [ $? -ne 0 ];then
   echo "Failed to run the sql to new database from $OUTDIR/premerge.sql"
   exit 1
fi

echo "Executing the Actual script "

{
for TT in $TABLES
do
    echo "table : $TT"

    if [[ $TT == *"cross_post_exploded_"* ]]; then
         #T=$(echo $TT | sed -e 's/instagram_crawl.cross_post_exploded_//g')
         T=$(echo $TT | sed -e 's/cross_post_exploded_//g')
         datasqlfile=$(echo $T.xpostsql.gz)
         querysqlfile=$(echo $T.xpost.sql)
    else if [[ $TT == *"cross_comment_exploded_"* ]]; then
            #T=$(echo $TT | sed -e 's/instagram_crawl.cross_comment_exploded_//g')
            T=$(echo $TT | sed -e 's/cross_comment_exploded_//g')
            datasqlfile=$(echo $T.xcommentsql.gz)
            querysqlfile=$(echo $T.xcomment.sql)
         else
            T=$(echo $TT | sed -e 's/instagram_crawl.entity_fan_//g')
            echo " This script is not used for this table $TT , use the regular script for $TT export to redshift. "
            exit
         fi
    fi
    
    echo "DATE : $T"
    echo "DataFile : $datasqlfile"
    echo "QueryFile : $querysqlfile"

    echo "DATA SQL [$datasqlfile] "
    echo "Checking For Already existing : [$OUTDIR/$datasqlfile] "

    if [[ ! -e "$OUTDIR/$datasqlfile" ]];then
        SEL="select A.dest_entity_uid as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '|', 1), 'A1-', '') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 2), '|', -1), 'A2-', '') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 3), '|', -1), 'A3-', '') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like 'http:%' then  REPLACE(REPLACE(post_link, 'http://instagram.com/p/', ''), '/', '') when post_link like 'https:%' then  REPLACE(REPLACE(post_link, 'https://instagram.com/p/', ''), '/', '') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, extract_or_create_uuid(A.userid) as uuid , ifnull(A.polarity,'') as polarity, ifnull(A.subjectivity,'') as subjectivity from $TT A INNER JOIN job_entities_$T B ON A.dest_entity_uid = B.aa_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON A.dest_entity_uid = E.entity_uid"
        echo "Working on $TT:"
        echo "$SEL;"
        echo "$SEL;" | mysql -u -n --quick -uroot -pindie -hlocalhost -A instagram_crawl | gzip -c > $OUTDIR/$datasqlfile && s3cmd put $OUTDIR/$datasqlfile s3://affinityanswers/incremental/instagram/
        if [ $? -eq 0 ];then
            echo "success";
        else
            echo "failure";
        fi
    else
        echo "FILE : $OUTDIR/$datasqlfile Already Exists , So not selecting the Data from DB Again " 
    fi
    
    echo " Creating the QueySQL to Load the data into redshift "
    {
        INS="copy incremental_ig from 's3://affinityanswers/incremental/instagram/$datasqlfile' credentials  'aws_access_key_id=AKIAIHW4MJPABIICZO7Q;aws_secret_access_key=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' delimiter '\\t' EMPTYASNULL GZIP IGNOREHEADER 1;"
        echo "BEGIN;"
        echo "$INS";
        echo "COMMIT;"
    } > $OUTDIR/$querysqlfile

    echo " Executing the QuerySQL to Load the data into redshift "
    ~/GonguraArchitecture/load-sql.sh $OUTDIR/$querysqlfile

    if [ $? -ne 0 ];then
        echo "Failed to run the sql to new database from $OUTDIR/$querysqlfile"
        exit 1
    else
        echo "UPDATE entity_fan_meta set in_new_warehouse = 'YES' where tablename = '$TT';" | mysql -uroot -pindie -hlocalhost -A instagram_crawl
        if [ $? -ne 0 ];then
            echo "Failed to update the status table"
            exit 1
        fi
    fi
done
} | tee $OUTDIR/$(date +%Y-%m-%d).log

echo "select count(*) from incremental_ig where create_time = 0;"; > $OUTDIR/postmerge.sql
echo "select count(*) from incremental_ig where systemtime = 0;"; >> $OUTDIR/postmerge.sql
echo "select count(*) from incremental_ig where profile_id = 0;"; >> $OUTDIR/postmerge.sql
echo "select count(*) from incremental_ig where postid is null;"; >> $OUTDIR/postmerge.sql

~/GonguraArchitecture/load-sql.sh $OUTDIR/postmerge.sql
if [ $? -ne 0 ];then
   echo "Failed to run the sql to new database from $OUTDIR/postmerge.sql"
   exit 1
fi

