#!/bin/bash

{
echo "create database web_stagram;"
echo "grant all privileges on web_stagram.* to web_stagram@localhost identified by 'webstagrampass';"
echo "flush privileges;"
} | mysql -uroot

