#!/usr/bin/bash

# checking the directory
# The following scripts needs to done from  /mnt/gonguradrive/instagram

LATESTWEEK=$1
LASTUPDATETIME=$2
PREVWEEKDATE=$3

        if [[ -z $1 || -z $2 || -z $3 ]]
        then 
        echo "USAGE : catalog-log-upload-local-test.sh $1 <YYYYmmmDD>"
        echo "Eg: catalog-upload-local-test.sh "2017jul12" '2017-07-12' '2017-07-05'"
            exit
            fi
echo "`date`"
#checking the events_ig counts from the redshift
echo "select count(*) from events_ig"
#echo "select count(*) from events_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting events_ig from redshit process has failed"
             exit
          else
             fi

#checking the incremental_ig counts from the redshift
echo "select count(*) from incremental_ig"
#echo "select count(*) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting incremental_ig from redshit process has failed"
             exit
          else
             fi

#checking the incremental_ig max/min (systemtime)
echo "select min(systemtime),max(systemtime) from incremental_ig"
#echo "select min(systemtime),max(systemtime) from incremental_ig" | env PGPASSWORD=ProdJunnontOrtIktom5 PGCLIENTENCODING=UTF-8 psql 'postgresql://ro_user@172.31.21.146:5439/gongura'
          if [ $? != 0 ]
          then
             echo "getting max/min (systemtime) from incremental_ig process has failed"
             exit
          else
             fi

#Update the Stats table in the aws
echo "sh -x getAndUpdateStats_aws.sh $LATESTWEEK"
#sh -x getAndUpdateStats_aws.sh $LATESTWEEK
          if [ $? != 0 ]
          then
             echo "UpdateStats into aws process has failed"
             exit
          else
             fi

#generating instagram-crawl error report and send mail to related persons
echo "sh -x generate-instagram-crawl-error-report-email.sh $LATESTWEEK"
#sh -x generate-instagram-crawl-error-report-email.sh $LATESTWEEK
          if [ $? != 0 ]
          then
             echo "Instagram-crawl error report send mail process has failed"
             exit
          else
             echo "Instagram-crawl error report mail has sent successfully"
             fi

#checking the count(*) from UUID_ig table status
echo "select count(*) from all_networks.UUID_ig"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select count(*) from all_networks.UUID_ig";
          if [ $? != 0 ]
          then
             echo "count(*) from UUID_ig status show process failed please look into this"
             exit
          else
             fi

#checking the max(uuid) from UUID_ig table status
echo "select max(uuid) from all_networks.UUID_ig"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select max(uuid) from all_networks.UUID_ig";
          if [ $? != 0 ]
          then
             echo "max(uuid) from UUID_ig status show process failed please look into this"
             exit
          else
             fi

#checking the count(*) from UUID_ig for last_updt_time table status
echo "select count(*) from all_networks.UUID_ig where last_updt_time > $LASTUPDATETIME"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select count(*) from all_networks.UUID_ig where last_updt_time > `$LASTUPDATETIME`";
          if [ $? != 0 ]
          then
             echo "count(*) from UUID_ig for last_updt_time table show process failed please look into this"
             exit
          else
             fi

#getting the status from ig_weekly_summary for the last week
echo "select * from ig_weekly_summary where cat_week = $PREVWEEKDATE"
#mysql -uroot -pindie -hlocalhost instagram_crawl -A "select * from ig_weekly_summary where cat_week = `$PREVWEEKDATE`";
          if [ $? != 0 ]
          then
             echo "ig_weekly_summary table status show process failed please look into this"
             exit
          else
             fi
echo "events_ig population and validations are done successfully"
echo "Goto Moby-dick loader3 and delete the instances"
echo "`date`"
