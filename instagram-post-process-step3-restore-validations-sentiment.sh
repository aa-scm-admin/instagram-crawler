#!/usr/bin/bash

# checking the directory
# The following scripts needs to done from  /mnt/gonguradrive/instagram

LATESTWEEK=$1

        if [[ -z $1 ]]
        then 
        echo "USAGE : catalog-log-upload-local-test.sh $1 <YYYYmmmDD>"
        echo "Eg: catalog-upload-local-test.sh "2017jul12" "
            exit
            fi

echo "`date`"
#restore db-data into aws mysql
echo "sh -x restore-ec2-db-data-in-aws-mysql.sh /mnt/gonguradrive/instagram/$LATESTWEEK/completed-backups/ `date`"
#sh -x restore-ec2-db-data-in-aws-mysql.sh /mnt/gonguradrive/instagram/$LATESTWEEK/completed-backups/
          if [ $? != 0 ]
          then
             echo "restore ec2 db-data in aws mysql process failed `date`"
             exit
          else
             echo "restored ec2 data into aws mysql has done successfully `date`"
             fi

#verifying the encrypted data
echo "sh -x verify-encrypted-data-integrity.sh"
#sh -x verify-encrypted-data-integrity.sh
          if [ $? != 0 ]
          then
             echo "verify encrypted-data-integrity process failed"
             exit
          else
             echo "verify encrypted data inegrity process has done successfully"
             fi

#moving into the home directory
cd /home/centos
        if [ $? != 0 ]
        then
           echo "directory has not changed - Operation Failed "
           exit 1
        else
           echo "directory has changed to /home/centos successfully "
        fi

#running the sentiment process on the entity_fan data
echo "python ig_sentiment.py entity_fan `date`"
#python ig_sentiment.py entity_fan
        if [ $? != 0 ]
        then
           echo "Sentiment process has failed `date`"
           exit 1
        else
           echo "Sentiment process has done successfully `date`"
        fi


#creating the latest job_entities table
echo "select count(*) , status, crawl_code from job_entities group by crawl_code"
mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) , status, crawl_code from job_entities group by crawl_code";
          if [ $? != 0 ]
          then
             echo "job_entities status show process failed"
             exit
          else
             fi

#creating the latest entity_fan table
echo "select count(*) from entity_fan where polarity is null AND fan_relationship like '%comment%'"
#sentcnt1=$(mysql -uroot -pindie -hlocalhost instagram_crawl -e "select count(*) from entity_fan where polarity is null AND fan_relationship like '%comment%';")
          if [ $sentcnt = 0 ]
          then
             echo "After sentment process entity_fan polarity and fan_relationship is null"
          else
             echo "After sentiment process still entity_fan polarity and fan_relationship is not null please look into this"
             exit 
             fi

#moving into the home directory
cd /mnt/gonguradrive/instagram

pwd

        if [ $? != 0 ]
        then
           echo "directory has not changed - Operation Failed "
           exit 1
        else
           echo "directory has changed to /mnt/gonguradrive/instagram successfully "
        fi

#restore db-data into aws mysql
echo "sh -x pre-export-script.sh"
#sh -x pre-export-script.sh
          if [ $? != 0 ]
          then
             echo "pre-export-script process failed"
             exit
          else
             echo "pre-export-script process has done successfully"
             fi

echo "`date`"
