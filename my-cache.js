/*
 * Author: Naresh <nareshv@colligent.com
 * Date  : Feb-2014
 *
 * (c) 2014 Naresh. All Rights Reserved.
 */
module.exports = {
    dir : __dirname + '/cache',
    path : function(url) {
        var hash = crypto.createHash('md5').update(url).digest('hex');
        return this.dir + '/' + hash + '.url';
    },
    exists : function(url) {
        if (fs.existsSync(this.path(url))) {
            return true;
        }
    },
    get : function(url) {
        return fs.readFileSync(this.path(url) + '.data');
    },
    put : function(url, content) {
        fs.writeFileSync(this.path(url), url);
        fs.writeFileSync(this.path(url) + '.data', content);
    }
};
