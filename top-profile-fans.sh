#!/bin/bash

if [ -z $1 ];then
    echo "syntax: $0 <catalogweek> <top-x-profiles>";
    exit;
fi

WEEK=$1
COUNT=50
table=instagram.entity_fan_$WEEK

echo "Checking index presence in $table";
LC=$(mysql -uinstagram -pinstagrampass -hstage5 -e"show index from $table" -t | grep -c userid)
if [ $LC -eq 0 ];then
    echo "Index doesn't exist creating new one ..."
    mysql -uinstagram -pinstagrampass -hstage5 -e'alter table entity_fan_2014jun4 add index (`userid`);' -t
else
    echo "Index already exist ... skipping index creation ..."
fi
echo "Top $COUNT Fans Report for $WEEK";
mysql -uinstagram -pinstagrampass -hstage5 -e"select count(distinct userid), profile_id from $table group by profile_id order by count(distinct userid) desc limit $COUNT;" -t
