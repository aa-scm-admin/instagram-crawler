#!/bin/bash


if [ -z $1 ];then
    echo "syntax: $0 <catalogweek>";
    exit
fi

catweek=$1

# Program do download the database files from remote EC2 nodes
echo "Started downloading data from remote EC2 server - $(date)"
for host in `awk '/db/ {print $3}' hosts-new.txt`
do
    echo Copying from $host
    mkdir /mnt/gonguradrive/instagram/$catweek

    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem root@$host:/disk/xvdg/completed-backup* /mnt/gonguradrive/instagram/$catweek
done

# Print details
echo "Completed. All backups are present under $(pwd)/completed-backups/ directory";
ls -l /mnt/gonguradrive/instagram/$catweek/completed-backups/

echo " Started Downloading the jsonData from Crawl Servers - $(date)"
for host in `awk '/crawl/ {print $3}' hosts-new.txt`
do
    crawlserver=`grep $host hosts-new.txt | cut -d " " -f 1` ; 
    echo Copying the Crawl json files from $host : $crawlserver 
    mkdir -p /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[0-9]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[a-n]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[o-z]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[A-N]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver
    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem centos@$host:[O-Z]*.json /mnt/gonguradrive/instagram/$catweek/$crawlserver

    #destfilename=`echo $catweek"_"$crawlserver`
    #tar -cvzf $destfilename /mnt/gonguradrive/instagram/$catweek/$crawlserver
    #s3cmd put $destfilename  s3://affinityanswers/instagram/
    #rm $destfilename

done

echo " Completed the Downloading Crawl Data - $(date) "

# print status
echo "All completed - $(date)."
