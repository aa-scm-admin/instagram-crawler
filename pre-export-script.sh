
#!/bin/bash

echo "Checking profiles which are still in crawling state ...";
mysql -uinstagram -hlocalhost -A instagram_crawl -pinstagrampass -t -e"select * from job_entities where status IN ('new', 'crawling')";

echo "Putting any left-over crawls to crawled state ...";
mysql -uinstagram -hlocalhost -A instagram_crawl -pinstagrampass -t -e"update job_entities set status = 'crawled' where status IN ('new', 'crawling')";

