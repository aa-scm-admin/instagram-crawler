#!/bin/bash

if [ -z $1 ];then
    echo "syntax $0 <dir>";
    exit;
fi

if [ ! -d $1 ];then
    echo "ERROR: $1 is not a directory. Please use proper directory.";
    echo "syntax $0 <dir>";
    exit
fi

dir=$1

echo "************** WARNING ************************"
echo "Please make sure you have renamed the following"
echo "tables in instagram_crawl database on stage5 "
echo ""
echo "-> job_entities"
echo "-> entity_fan"
echo ""
echo "************** WARNING ************************"
echo ""
echo -e "Are you sure to continue with restore ? (yes/no) : \c"
read yesno

if [[ $yesno == "yes" || $yesno == "y" ]];then
    echo "Started restore operation at $(date).."
    echo ""

    echo "1. Restoring job_entities ...";
    echo -e "1.1 Uncompressing .. : \c"
    if [ -f "$dir/job_entities.tsv" ]
    then
        echo "[skipped]"
    else
        bunzip2 "$dir/job_entities.tsv.bz2"
        if [ $? -eq 0 ];then
            echo "[completed]"
        else
            echo "[failure]"
            exit 1;
        fi
    fi
    echo -e "1.2 Restoring ... : \c"
    #mysql -uinstagram -pinstagrampass -A -hstage5 instagram_crawl -e"ALTER IGNORE TABLE job_entities RENAME job_entities_safe_backup"
    #mysql -uinstagram -pinstagrampass -A -hstage5 instagram_crawl -e"SOURCE $dir/instagram_crawl.job_entities.sql;"
    mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_crawl -e"load data local infile '$dir/job_entities.tsv' into table instagram_crawl.job_entities character set utf8mb4;"
    if [ $? -eq 0 ];then
        echo "[completed]"
    else
        echo "[failure]"
        exit 1;
    fi
    echo
    echo

    echo "2. Restoring entity_fan ...";
    echo -e "2.1 Uncompressing ... : \c"
    if [ -f "$dir/entity_fan.tsv" ]
    then
        echo "[skipped]"
    else
        bunzip2 "$dir/entity_fan.tsv.bz2"
        if [ $? -eq 0 ];then
            echo "[completed]"
        else
            echo "[failure]"
            exit 1;
        fi
    fi
    echo -e "2.2 Restoring ... : \c"
    #mysql -uinstagram -pinstagrampass -A -hstage5 instagram_crawl -e"ALTER IGNORE TABLE entity_fan RENAME entity_fan_safe_backup;"
    #mysql -uinstagram -pinstagrampass -A -hstage5 instagram_crawl -e"SOURCE $dir/instagram_crawl.entity_fan.sql;"
    mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_crawl -e"load data local infile '$dir/entity_fan.tsv' into table instagram_crawl.entity_fan character set utf8mb4;"
    if [ $? -eq 0 ];then
        echo "[completed]"
    else
        echo "[failure]"
        exit 1;
    fi
    echo
    echo

    echo "3. Restoring fan_universe ...";
    echo -e "3.1 Uncompressing ... : \c"
    if [ -f "$dir/fan_universe.tsv" ]
    then
        echo "[skipped]"
    else
        bunzip2 "$dir/fan_universe.tsv.bz2"
        if [ $? -eq 0 ];then
            echo "[completed]"
        else
            echo "[failure]"
            exit 1;
        fi
    fi
    echo -e "3.2 Restoring ... : \c"
    mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_universe -e"load data local infile '$dir/fan_universe.tsv' replace into table instagram_universe.fan_universe character set utf8mb4;"
    if [ $? -eq 0 ];then
        echo "[completed]"
    else
        echo "[failure]"
        exit 1;
    fi
    echo
    echo

    echo "All complete. $(date)"
else
    echo "---> Did not continue ...."
fi
