/*
 * Author: Naresh <nareshv@colligent.com
 * Date  : Feb-2014
 *
 * (c) 2014 Naresh. All Rights Reserved.
 */

module.exports = {
    headers : [
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)',
        'Accept-Encoding: gzip, deflate',
        'Cache-Control: max-age=0',
        'Accept-Language: en-US,en;q=0.5'
    ],
    AJAXheaders : function(referer) {
        return [
            'Host: web.stagram.com',
            'Accept: */*',
            'Accept-Encoding: gzip, deflate',
            'Accept-Language: en-US,en;q=0.5',
            'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0',
            'Cookie: __cfduid=d6040b8fb9491b3d34046661b872694b21391572971710; PHPSESSID=gfl9a4dtemi5g96a3lsrolhke3; webstagram_fb_box_viewed=1',
            'Referer: ' + referer,
            'X-Requested-With: XMLHttpRequest'
        ]
    },
    crawl_options : {
        delay : 16 * 1000,
        next_date  : new Date()
    },

// Top 100 Instagram accounts from
// Page: http://web.stagram.com/hot
//
// Jquery Scrape code
/*
    var i = 0;
    var j = [];
    var c = '';
    $('div.results').find('tr').each(function() {
      i++;
      c = $(this).find('td:eq(2) strong').text();
      if (c.match(/^[a-z]+/)) { j.push(c); }
    });

    console.log(JSON.stringify(j));
*/


    top100 : [
        "instagram",/*"justinbieber","kimkardashian"*/,"badgalriri",/*"beyonce", "mileycyrus",*/ "arianagrande","kendalljenner","khloekardashian","taylorswift",
        "kyliejenner","kourtneykardash","selenagomez","harrystyles","kevinhart4real","camposwell","niallhoran","champagnepapi","kingjames",
        "onedirection","snookinic","akon","kinggoldchains","caradelevingne","nickiminaj","natgeo","victoriassecret","ddlovato","neymarjr",
        "katyperry","chiragchirag78","cristiano","theellenshow","justintimberlake","nike","itsashbenzo","iamdiddy","mirandakerr","austinmahone",
        "letthelordbewithyou","lucyhale","ladygaga","channingtatum","shaym","laurenconrad","jessicaalba","macklemore","williejae","therock",
        "zendayamaree","barackobama","mistercap","dwyanewade","louist91","brunomars","forever21","snoopdogg","zooeydeschanel","angelcandices","bestvines",
        "nashgrier","krisjenner","ciara","jennamarbles","vanessahudgens","robkardashian","bellathorne","oprah","britneyspears","taliajoy18",
        "bethanynoelm","floydmayweather","fuckyopictures","jenselter","leomessi","aliciakeys","lenitsky","tyrabanks","easymoneysniper","fashiiononfire",
        "real_liam_direction","meekmill","louboutinworld","sheksson","thekatvond","xxxibgdrgn","parishilton","brumarquezine","elliegoulding","starbucks",
        "therealcharlieb","kobebryant","lala","ninadobrev","victoriajustice","mtv","codysimpson","aum_patchrapa","iansomerhalder","photogeekdom",
        "izdato_eng","toppeopleworld","eyemediaa","igtoppicture","ivmikspb","igtoppics","girlscar","dianchik2196","maxwhitee","rftrends",
        "webix","ruredaktor","favoritmusic","besttrends","fotogasm","keysik","niksidorkin","allpaparazzi","fashiiononfire","zalezakaofficial",
        "zzkoda","fashion_creative_love","loveigshouts","patricknorton","barcelonacitizen","leah_rasheed","foreverstyled","animalwow","netofernandez7","wtf",
        "cre8magic","neznashka","oliverglass","foqasa","funsubstance","fashiionfix","inst_chic","kazabby","daretothread","denusko",
        "loveindianhair","fitness_elites","marshanskiy","mosvidos_ru","k_chugunkin","eyestopper","sazam_eng","tesoroom","kidfella","instahaiku",
        "go","brutalcactus","ryashskihh","jaybling","alukoyanov","gothiphop","lindadoremi","dudubarina","yellow","igsg",
        "hangarang","ironbrain389","svetlana_24","mapmelad","funymoney","ilya_sinus","allthebestyo","emil_valentino","vacuat6","beautifulworldgroup",
        "warmwednesday","azary07","magickmondays","rushsfz","chaturbat","jackfrase","studiolegalefrigerio","karrimovrussia","amazing_pretty","svvaplife",
        "carpron","ivanlysak","negin_mirsalehi","aidemftw","ig_fitsporation","alexsydneymagic","svetakozhevnikova","highonlifeco","autopron","hruckerdotcom",
        "eyecandymag","mediawhite","amclassiccars","thekillertruth","necyd","animegirls777","pin38","olga4u"
    ]
}
