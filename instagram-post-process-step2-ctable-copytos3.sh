#!/usr/bin/bash

# checking the directory
# The following scripts needs to done from  /mnt/gonguradrive/instagram

PREVIOUSWEEK=$1
LATESTWEEK=$2

        if [[ -z $1 || -z $2 ]]
        then 
        echo "USAGE : catalog-log-upload-local-test.sh $1 <YYYYmmmDD>"
        echo "Eg: catalog-upload-local-test.sh "2017jul05" "2017jul12" "
            exit
            fi

echo "`date`"
#creating the latest job_entities table
echo "create table job_entities like job_entities_$PREVIOUSWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "create table job_entities like job_entities_$PREVIOUSWEEK";
          if [ $? != 0 ]
          then
             echo "job_entities table creation process failed"
             exit
          else
             fi

#creating the latest entity_fan table
echo "create table entity_fan like entity_fan_$PREVIOUSWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "create table entity_fan like entity_fan_$PREVIOUSWEEK";
          if [ $? != 0 ]
          then
             echo "entity_fan table creation process failed"
             exit
          else
             fi

#creating the latest posts table
echo "create table instagram_universe.post like  instagram_universe.post_$PREVIOUSWEEK"
#mysql -uroot -pindie -hlocalhost instagram_crawl -e "create table instagram_universe.post like  instagram_universe.post_$PREVIOUSWEEK";
          if [ $? != 0 ]
          then
             echo "posts table creation process failed"
             exit
          else
             fi

#copy the data from ec2 to aws
echo "sh -x copy-db-data-from-ec2-to-aws.sh $LATESTWEEK `date`"
#sh -x copy-db-data-from-ec2-to-aws.sh $LATESTWEEK
          if [ $? != 0 ]
          then
             echo "Copy DB data from ec2 to aws process failed"
             exit
          else
             echo "Copy DB data from ec2 to aws has done successfully `date`"
             fi


echo "Copied crawl dato S3 process Done"
echo "`date`"
