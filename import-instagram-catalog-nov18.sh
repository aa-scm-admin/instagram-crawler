#/bin/bash

#
# -bash-3.2$ ./import-instagram-catalog.sh /ccdata/catalog/2014may21/Entity_Catalog_Instagram.txt
# === outfile : /tmp/Entity_Catalog_Instagram.txt ===
# Written 6067 lines to /tmp/Entity_Catalog_Instagram.txt
#

if [[ -z $1 ]];then
    echo "syntax: $0 [catalog-week] <path/to/file/for/prod/mode>"
    exit
fi

week=$1
db=instagram_crawl
table=$db.job_entities
host=localhost
user=instagram
pass=instagrampass

if [[ -z $2 ]];then
    rfile="/ccdata/catalog/$week/Entity_Catalog.txt"
    file=$(basename $rfile)
    echo "Downloading $rfile from proc4 server ....";
    scp -C proc4.colligent.com:$rfile $file
else
    file=$2
    echo "Enabled production mode. Not downloading file .."
fi

filename=`basename $file`
outfile=/tmp/$filename

if [ ! -f $file ]
then
    echo "Error: $file does not exist."
    exit 1;
fi

if [[ $filename == "Entity_Catalog_Instagram.txt" ]];then
    #Serial No.      Tracked Category        Entity Name     Attribute 1 Code        Attribute 2 Code        Attribute 3 Code        MS url  MS id   FB url  FB id   TW url  TW id   gp url  gp id  search_str       bk_name instagram id    instagram URL
    #rm -f $outfile && awk -F"\t" 'BEGIN{OFS = FS} {if (length($18) > 1) print $1,$3,$18,$19; }' < $file > $outfile
    rm -f $outfile && cat $file | perl -le 'sub _t {my $a = shift; $a =~ s/^\W+|\W+$//ig; return $a;} while(<>) { chomp; @a = split(/\t/, $_); if ($a[1] eq "Y" and length($a[17]) > 1) { @q = (_t($a[0]),_t($a[2]),_t($a[17]),_t($a[18])); print join(",", @q); } }' > $outfile
elif [[ $filename == "Entity_Catalog.txt" ]];then
    #Serial No.      Tracked Category        Entity Name     Attribute 1 Code        Attribute 2 Code        Attribute 3 Code        instagram URL   instagram id    FB url  FB id   TW url  TW id  gp url   gp id   search_str      bk_name
    #rm -f $outfile && awk -F"\t" 'BEGIN{OFS = FS} {if (length($8) > 1) print $1,$3,$9,$8; }' < $file > $outfile
    rm -f $outfile && cat $file | perl -le 'sub _t {my $a = shift; $a =~ s/^\W+|\W+$//ig; return $a;} while(<>) { chomp; @a = split(/\t/, $_); if ($a[1] eq "Y" and length($a[7]) > 1) { @q = (_t($a[0]),_t($a[2]),_t($a[8]),_t($a[7]),_t($a[17])); print join(",", @q); } }' > $outfile
else
    echo "Invalid catalog file : $filename"
    exit 1
fi

echo "=== outfile : $outfile ==="
#head -2 $outfile | od -bc

if [ ! -z $outfile ];then
    echo "Written `wc -l < $outfile` lines to $outfile"
    {
        echo 'DROP TABLE IF EXISTS '$table';';
        echo 'CREATE TABLE '$table' (';
        cat <<'EOM'
  `aa_id` int(10) unsigned NOT NULL,
  `category` varchar(3) NOT NULL,
  `profile_id` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `catalog_week` DATE NOT NULL,
  `num_http_reqs` int(10) unsigned DEFAULT '0',
  `num_posts` int(10) unsigned DEFAULT '0',
  `num_users` int(10) unsigned DEFAULT '0',
  `num_comments` int(10) unsigned DEFAULT '0',
  `num_likes` int(11) DEFAULT '0',
  `vid_comments` int(11) DEFAULT '0',
  `vid_likes` int(11) DEFAULT '0',
  `global_posts` int(11) DEFAULT '0',
  `global_followers` int(11) DEFAULT '0',
  `global_following` int(11) DEFAULT '0',
  `status` ENUM('new', 'crawling', 'crawled', 'merging', 'merged', 'validated', 'crawlerror', 'mergeerror', 'validationerror') NOT NULL DEFAULT 'new',
  `max_post_age` INTEGER UNSIGNED NOT NULL DEFAULT '864000',
  `crawl_code` varchar(32) DEFAULT NULL,
  `crawl_hostname` varchar(16) DEFAULT NULL,
  `crawl_start_time` datetime DEFAULT NULL,
  `crawl_end_time` datetime DEFAULT NULL,
  `crawl_pid` int(11) DEFAULT NULL,
  `merge_hostname` varchar(16) DEFAULT NULL,
  `merge_start_time` datetime DEFAULT NULL,
  `merge_end_time` datetime DEFAULT NULL,
  `merge_pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`aa_id`),
  UNIQUE KEY `profile_id` (`profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
EOM
        echo 'LOAD DATA LOCAL INFILE "'$outfile'" INTO TABLE '$table' COLUMNS TERMINATED BY "," LINES TERMINATED BY "\n" IGNORE 1 LINES (aa_id, category, profile_id, url, @catalog_week) set catalog_week = STR_TO_DATE(@catalog_week, "%e-%b-%Y");';
        echo 'UPDATE '$table' SET status = "new", crawl_hostname = NULL, crawl_start_time = NULL, crawl_end_time = NULL, merge_hostname = NULL, merge_start_time = NULL, merge_end_time = NULL, num_posts = 0, num_users = 0, num_comments = 0, num_likes = 0, crawl_pid = NULL, global_posts = 0, global_followers = 0, global_following = 0;';
        # Set the age of new profiles, to be a full-crawl
        echo "UPDATE job_entities set max_post_age=0 where unix_timestamp(str_to_date('$week', '%Y%b%e')) - unix_timestamp(date(catalog_week)) <= 86400 * 7;";
        # Set the age of old profiles, to be a deep crawl
        echo "UPDATE job_entities set max_post_age = (86400 * 30) where unix_timestamp(str_to_date('$week', '%Y%b%e')) - unix_timestamp(date(catalog_week)) > 86400 * 7;";
        # Print the summary after updates are complete.
        echo "SELECT COUNT(*) FROM $table;"
        echo "SELECT * FROM $table LIMIT 1\G"
    } > /tmp/restore.sql
    cat /tmp/restore.sql
    mysql -u$user -p$pass -h$host -A $db < /tmp/restore.sql

    echo
    echo "---------> profiles and their ages <-------------";
    mysql -t -u$user -p$pass -h$host -A $db -e'select count(*), status, catalog_week, max_post_age from job_entities group by status, catalog_week, max_post_age;'
    echo
    echo "---------> total profiles for this week <--------";
    mysql -t -u$user -p$pass -h$host -A $db -e"SELECT COUNT(*) FROM $table;"
    echo

    exit 0
else
    echo "Error: Failed to create $outfile. job_entities table is not populated."
    exit 1
fi
